import * as Koa from "koa"

export const app = new Koa();
import { userRouter } from "./routers/user";
import { messageRouter } from "./routers/message";
import { interviewRouter } from "./routers/interview";
import { talentRouter } from "./routers/talent";
import * as logger from "koa-logger";
import * as  koabody from "koa-body";
import * as passport from "koa-passport"
import * as session from "koa-session"
import * as mongoose from "mongoose"
import * as path from "path";
import serve = require("koa-static");
// import * as path from "path";

export let connection = mongoose.connect("mongodb://localhost/talentSeeker" || "mongodb://anteneh845:Pass1234@ds257097.mlab.com:57097/talent-seeker");
// export let connection = mongoose.connect( "mongodb://anteneh845:Pass1234@ds257097.mlab.com:57097/talent-seeker");

var port = process.env.PORT || 8000;
app.use(koabody({ multipart: true }));
require("./config/passport");

app.keys = ['Talent seeker keys']; // todo change keys
app.proxy = true;
app.use(session(app));
app.use(passport.initialize());
app.use(passport.session());
app.use(logger());


app.use(userRouter.routes());
app.use(messageRouter.routes());
app.use(talentRouter.routes());
app.use(interviewRouter.routes());

if (process.env.NODE_ENV === "production") {
    app.use(serve(path.join(process.cwd(), "..", 'web', 'build')));
    console.log(path.join(process.cwd(), 'web', 'build'));
    console.log("Started in production")
}

app.listen(port, function () {
    console.log("Talent seeker listening: " + process.env.NODE_ENV);
})
