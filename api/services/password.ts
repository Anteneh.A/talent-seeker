import { KoaError } from "./koa-error";
import { compare, hash } from 'bcrypt'
import { Schema } from "mongoose";
import { User } from "../models/user";
import { get } from "./crud";
export async function setPassword(password: string): Promise<string> {
    if (!password) throw new KoaError('"password" parameter not found', 400, 'NO_PASS')

    if (password.length > 72)
        throw new KoaError(
            'Password cannot be longer than 72 characters.',
            400,
            'PASS_TOO_LONG'
        )

    if (!password.match(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/))
        throw new KoaError(
            'Password needs to be at least 8 characters long and contain at least one of capital letters, small letters and numbers each.',
            400,
            'PASS_VALIDATION_FAILED'
        )
    password = await hash(password, 14)
    return password;
}
export async function changePassword(
    currentPassword: string,
    newPassword: string,
    _id: Schema.Types.ObjectId
): Promise<void> {
    if (!currentPassword)
        throw new KoaError('"currentPassword" parameter not found', 400, 'NO_CURRENT_PASS')
    if (!newPassword)
        throw new KoaError('"newPassword" parameter not found', 400, 'NO_NEW_PASS')


    if (!(await isPasswordCorrect(currentPassword, _id)))
        throw new KoaError('Incorrect password.', 401, 'INCORRECT_PASSWORD')
}


export async function isPasswordCorrect(password: string, _id: Schema.Types.ObjectId): Promise<boolean> {
    const user: any = await get(User, _id); //todo 
    if (!password)
        throw new KoaError('"password" parameter not found', 400, 'NO_PASS')

    return compare(password, user.password)
}