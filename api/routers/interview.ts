import * as Router from "koa-router";

const interviewRouter = new Router({ prefix: "/api/interview" });

interviewRouter.post('', async ctx => {
    ctx.body = "Interview request";
})

interviewRouter.put('/:_id', async ctx => {
    ctx.body = "Edits interview ";
})

interviewRouter.get('/:_id', async ctx => {
    ctx.body = "Gets interviews";
})

interviewRouter.delete('/:_id', async ctx => {
    ctx.body = "Deletes interview ";
})

export { interviewRouter };