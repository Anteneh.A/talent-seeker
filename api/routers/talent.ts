import * as Router from "koa-router";
import {
    EditTalent,
    GetTalent,
    DeleteTalent,
    AddTalent,
    AddSkill,
    AddEducation,
    AddEmployment,
    AddPortfolio,
    DeletePortfolio,
    DeleteEducation,
    DeleteEmployment,
    DeleteSkill,
    EditPortfolio,
    EditEducation, EditEmployment, EditSkill, RateUser, Search, GetUserTalent
} from "../controllers/talent";

export const talentRouter = new Router({ prefix: "/api/talent" });

talentRouter.post('/add', async ctx => {
    ctx.body = await AddTalent(ctx.state.user._id, ctx.request.body);
});

talentRouter.get('/user/:_id', async ctx => {
    ctx.body = await GetUserTalent(ctx.params._id);
});

/*talentRouter.get('/search', async ctx => {
    ctx.body = await Search("React");
});*/

talentRouter.get('/mine', async ctx => {
    ctx.body = await GetTalent(ctx.state.user._id);
});

talentRouter.put('/:_id', async ctx => {
    ctx.body = await EditTalent(ctx.params._id, ctx.request.body);
});

talentRouter.put('/skill/add', async ctx => {
    ctx.body = await AddSkill(ctx.request.body, ctx.state.user._id);
});

talentRouter.put('/education/add', async ctx => {
    ctx.body = await AddEducation(ctx.request.body, ctx.state.user._id);
});

talentRouter.put('/employment/add', async ctx => {
    ctx.body = await AddEmployment(ctx.request.body, ctx.state.user._id);
});

talentRouter.put('/portfolio/add', async ctx => {
    ctx.body = await AddPortfolio(ctx.request.body, ctx.state.user._id);
});

talentRouter.delete('/:_id/portfolio/:_portfolioID', async ctx => {
    ctx.body = await DeletePortfolio(ctx.params._id, ctx.params._portfolioID);
}
);

talentRouter.delete('/:_id/education/:_educationID', async ctx => {
    ctx.body = await DeleteEducation(ctx.params._id, ctx.params._educationID);
});

talentRouter.delete('/:_id/employment/:_employmentID', async ctx => {
    ctx.body = await DeleteEmployment(ctx.params._id, ctx.params._employmentID);
});

talentRouter.delete('/:_id/skill/:skillID', async ctx => {
    ctx.body = await DeleteSkill(ctx.params._id, ctx.params.skillID);
});

talentRouter.put('/:_id/portfolio/:_portfolioID', async ctx => {
    ctx.body = await EditPortfolio(ctx.params._id, ctx.params._portfolioID, ctx.request.body);
});

talentRouter.put('/:_id/education/:_educationID', async ctx => {
    ctx.body = await EditEducation(ctx.params._id, ctx.params._educationID, ctx.request.body);
});

talentRouter.put('/:_id/employment/:_employmentID', async ctx => {
    ctx.body = await EditEmployment(ctx.params._id, ctx.params._employmentID, ctx.request.body);
});

talentRouter.put('/:_id/skill/:skillID', async ctx => {
    ctx.body = await EditSkill(ctx.params._id, ctx.params.skillID, ctx.request.body);
});

talentRouter.delete('/:_id', async ctx => {
    ctx.body = await DeleteTalent(ctx.params._id)
});

talentRouter.post('/:_id/review', async ctx => {
    console.log("Review user");
    ctx.body = await RateUser(ctx.params._id, ctx.request.body, ctx.state.user._id);
});

// todo Advanced Search for talent

talentRouter.get('/search/:query', async ctx => {
    console.log("was here")
    ctx.body = await Search(ctx.params.query);
    
})