import * as Router from "koa-router";
import {
    AddUser,
    EditUserProfile,
    GetUser,
    DeleteUser,
    ListUser,
    ChangePassword,
    addPicture,
    getPicture, deletePicture
} from "../controllers/user";
import * as passport from "koa-passport";

const userRouter = new Router({ prefix: "/api/user" });

userRouter.get("/all", async ctx => {
    ctx.body = await ListUser();
});

userRouter.post("/register", async ctx => {
    ctx.body = await AddUser(ctx.request.body);
});

userRouter.post(
    "/login",
    passport.authenticate("local", {
        successRedirect: "/",
        failureRedirect: "/login?error=Invalid email or password"
    })
);

userRouter.post("/logout", async ctx => {
    if (ctx.isAuthenticated) {
        console.log("authenticated");
        ctx.logout();
        ctx.body = {loggedOut: true};
    }
});

userRouter.get("/me", async ctx => {
    console.log("Was here /me");
    console.log(ctx.state.user._id);
    ctx.body = await GetUser(ctx.state.user._id);
});

userRouter.post("/picture/:_id", async ctx => {
    console.log(ctx.request.files!.picture.path);
    ctx.body = await addPicture(
        ctx.request.files!.picture.path,
        ctx.params._id
    );
});
userRouter.get("/picture/:_id", async ctx => {
    ctx.body = await getPicture(ctx.params._id);
});

userRouter.get("/picture/remove/:_id", async ctx => {
    ctx.body = await deletePicture(ctx.params._id);
});

userRouter.put("/password", async ctx => {
    ctx.body = await ChangePassword(ctx.request.body);
});

userRouter.put("/:_id", async ctx => {
    ctx.body = await EditUserProfile(ctx.request.body, ctx.params._id);
});
userRouter.delete("/:_id", async ctx => {
    ctx.body = await DeleteUser(ctx.params._id);
});

export { userRouter };

// todo Searching a user
