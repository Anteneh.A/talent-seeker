import * as Router from "koa-router";
import {
    SendMessage,
    EditMessage,
    DeleteMessage,
    ListAllMessage,
    GetMessage
} from "../controllers/message";

const messageRouter = new Router({ prefix: "/api/message" });

messageRouter.get('/list', async ctx => {
    ctx.body = await ListAllMessage(ctx.state.user._id);
});

messageRouter.post('/send/:_id', async ctx => {
    ctx.body = await SendMessage(ctx.state.user._id, ctx.params._id, ctx.request.body);
});

messageRouter.put('/:_id', async ctx => {
    ctx.body = await EditMessage(ctx.params._id, ctx.request.body);
});

messageRouter.get('/:_id', async ctx => {
    ctx.body = await GetMessage(ctx.params._id);
});

messageRouter.delete('/:_id', async ctx => {
    console.log("Deleting message");
    ctx.body = await DeleteMessage(ctx.params._id, ctx.state.user._id);
});


export { messageRouter };