import * as mongoose from "mongoose";
import { Schema } from 'mongoose';

export interface IInterview {
    description: string
    date: Date
    employeeId: Schema.Types.ObjectId
    recruiterId: Schema.Types.ObjectId
}

const InterviewSchema = new Schema({
    _at: { type: Date, default: Date.now },
    description: { type: String, required: true },
    date: { type: Date, required: true },
    employeeId: { type: Schema.Types.ObjectId, refs: "User" },
    recruiterId: { type: Schema.Types.ObjectId, refs: "User" },
    acceptance: { type: Boolean }
});
const Interview = mongoose.model("Interview", InterviewSchema);
export { Interview }
