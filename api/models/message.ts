import * as mongoose from "mongoose";
import { Schema } from 'mongoose';

export interface IMessage {
    user: Schema.Types.ObjectId
    message: string
    title: string
    to: Schema.Types.ObjectId | null
    seen: boolean,
    deleted: boolean
    date?: Date
}

const MessageSchema = new Schema({
    date: { type: Date, required: true, default: Date.now() },
    user: { type: Schema.Types.ObjectId, refs: "User" },
    message: { type: String, required: true, index: true },
    title: { type: String, required: true, index: true },
    to: { type: Schema.Types.ObjectId || null, refs: "User" },
    seen: { type: Boolean, default: false },
    deleted: { type: Boolean, default: false }
});
MessageSchema.index({ message: 'text' });

const Message = mongoose.model("Message", MessageSchema);
export { Message }
