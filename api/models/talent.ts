import * as mongoose from "mongoose";
import { Document, Schema } from "mongoose";

export interface ITalent extends Document {
  userId?: Schema.Types.ObjectId;
  bio?: string;
  skills?: [{ skill: string; level: string }];
  portfolio?: [
    {
      project: string;
      description: string;
      link: string;
    }
  ];
  employment?: [
    {
      company: { type: string; required: true };
      startDate: Date;
      endDate: Date;
      jobTitle: string;
      description: string;
    }
  ];
  education?: [
    {
      school: string;
      field: string;
      startDate: Date;
      endDate: Date;
    }
  ];
  availability?: {
    available: boolean;
    availabilityDate: Date;
  };
  review?: [
    {
      _at: Date;
      rating: number;
      review: string;
      from: Schema.Types.ObjectId;
    }
  ];
}

// todo change to enum
// const level=['BEGINNER','ADVANCED','EXPERT']

// type Ilevel:level=['BEGINNER'|'ADVANCED'| 'EXPERT']

const TalentSchema = new Schema({
  createdOn: { type: Date, default: Date.now() },
  userId: { type: Schema.Types.ObjectId, required: true, refs: "User" },
  bio: { type: String },
  availability: {
    available: { type: Boolean, default: true },
    availabilityDate: { type: Date }
  },
  skills: [
    {
      skill: { type: String, index: true },
      level: { type: String }
    }
  ],
  portfolio: [
    {
      project: { type: String, index: true },
      description: { type: String },
      link: { type: String }
    }
  ],
  employment: [
    {
      company: { type: String, index: true },
      startDate: { type: Date },
      endDate: { type: Date },
      jobTitle: { type: String, index: true },
      description: { type: String }
    }
  ],
  education: [
    {
      school: { type: String, index: true },
      field: { type: String, index: true },
      startDate: { type: Date },
      endDate: { type: Date }
    }
  ],
  review: [
    {
      _at: { type: Date, default: Date.now() },
      rating: { type: Number },
      review: { type: String },
      from: { type: Schema.Types.ObjectId }
    }
  ]
});
TalentSchema.index({
  "portfolio.project": "text",
  "skills.skill": "text",
  "education.school": "text",
  "education.field": "text",
  "employment.company": "text",
    "employment.field": "text",
});

const Talent = mongoose.model("Talent", TalentSchema);
/*Talent.ensureIndexes({
 "portfolio.project": "text",
   "skills.skill": "text",
   "education.school": "text",
   "education.field": "text",
   "employment.company": "text",
   "employment.field": "text",
}
)*/
export { Talent };
