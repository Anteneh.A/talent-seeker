import * as mongoose from "mongoose";
import { Document, Schema } from 'mongoose';

export interface IUser extends Document {
    _id: Schema.Types.ObjectId
    name: string
    message: Schema.Types.ObjectId
    email: string
    phone: string
    talent?: Schema.Types.ObjectId
    messages?: [Schema.Types.ObjectId]
    interview?: [Schema.Types.ObjectId]
    password: string
}


const UserSchema = new Schema({
    createdOn: { type: Date, required: true, default: Date.now() },
    name: { type: String, required: true, index: true },
    message: { type: Schema.Types.ObjectId },
    phone: { type: String, required: true, index: true },
    email: { type: String, required: true, unique: true, index: true },
    password: { type: String, required: true },
    role: {type: String},// todo change these to enum
    talent: { type: Schema.Types.ObjectId, refs: "Talent" },
    interview: [{ type: Schema.Types.ObjectId }],
    messages: [{ type: Schema.Types.ObjectId, ref: "Message" }]
});
UserSchema.index({ name: 'text', phone: 'text', email: 'text' });

const User = mongoose.model("User", UserSchema);
export { User }
