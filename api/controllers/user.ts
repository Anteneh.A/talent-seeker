import { IUser, User } from "../models/user";
import { Schema, Document } from "mongoose";
import { add, edit, remove, get, list } from "../services/crud";
import { setPassword } from "../services/password";
import { Talent } from "../models/talent";
import {hash} from "bcrypt";
import * as fs from "fs";
import * as  Grid from "gridfs-stream";
import * as mongoose from "mongoose";

export async function ListUser(): Promise<any> {
    let users: any = await list(User);
    let talents: any = [];
    for (let i = 0; i < users.length; i++)
        talents.push(await get(Talent, users[i]!.talent));
    return {user: users, talent: talents};
}

export async function AddUser(body: IUser): Promise<void> {
    body.password = await setPassword(body.password);
    let user: any = await add(User, body);
    let talent: any = {
        userId: user._id
    };
    let t = await add(Talent, talent);
    user.talent = t._id;
    user.save();
}

export async function EditUserProfile(
    body: any,
    _id: Schema.Types.ObjectId
): Promise<void> {
    console.log(body);
    await edit(User, _id, body.account);
    // User.findByIdAndUpdate(_id,body);
}

export async function DeleteUser(_id: Schema.Types.ObjectId): Promise<void> {
    await remove(User, _id);
}

export async function ChangePassword(body: any): Promise<any> {
    body.account.password = await hash(body.account.password, 14);
    return await edit(User, body.account._id, body.account);
}

export async function GetUser(
    _id: Schema.Types.ObjectId
): Promise<IUser | Document> {
    return await get(User, _id);
}

export async function addPicture(
    path: any,
    _id: Schema.Types.ObjectId
): Promise<void> {
    let gfs = Grid(mongoose.connection.db, mongoose.mongo);
    gfs.exist({filename: "" + _id}, function (err, found) {
        if (err) console.log(err);
        found ? gfs.remove({filename: "" + _id}, function (err) {
            if (err)
                console.log(err);
        }) : console.log('File does not exist');
    });
    fs.createReadStream(path).pipe(gfs.createWriteStream({filename: "" + _id}));
    return Promise.resolve();
}

export async function getPicture(
    _id: Schema.Types.ObjectId
): Promise<any> {
    let gfs = Grid(mongoose.connection.db, mongoose.mongo);
    return gfs.createReadStream({filename: "" + _id});
}

export async function deletePicture(_id: Schema.Types.ObjectId): Promise<void> {
    let gfs = Grid(mongoose.connection.db, mongoose.mongo);
    gfs.remove({filename: "" + _id}, function (err) {
        if (err)
            console.log(err);
    });
}
