import { Schema, Document } from "mongoose";
import { ITalent, Talent } from "../models/talent";
import { get, edit, remove, add } from "../services/crud";
import { User } from "../models/user";

export async function GetTalent(
  _id: Schema.Types.ObjectId
): Promise<ITalent | Document> {
  let user: any = await get(User, _id);
  return await get(Talent, user.talent);
}

export async function EditTalent(
  _id: Schema.Types.ObjectId,
  body: ITalent
): Promise<void> {
  await edit(Talent, _id, body);
}
export async function DeleteTalent(_id: Schema.Types.ObjectId): Promise<void> {
  await remove(Talent, _id);
}
export async function AddTalent(
  _id: Schema.Types.ObjectId,
  body: ITalent
): Promise<void> {
  body.userId = _id;
  await add(Talent, body);
}

export async function AddSkill(body: any, _id: any): Promise<void> {
  console.log(body);
  let user = await get(User, _id);
  //@ts-ignore
  let talent: any = await get(Talent, user.talent);
  console.log(talent);
  await talent.skills.push(body);
  await talent.save();
  console.log("exited");
}

export async function AddEducation(body: any, _id: any): Promise<void> {
  console.log(body);
  let user = await get(User, _id);
  //@ts-ignore
  let education: any = await get(Talent, user.talent);
  console.log(education);
  await education.education.push(body);
  await education.save();
  console.log("exited");
}

export async function AddEmployment(body: any, _id: any): Promise<void> {
  console.log(body);
  let user = await get(User, _id);
  //@ts-ignore
  let employment: any = await get(Talent, user.talent);
  console.log(employment);
  await employment.employment.push(body);
  await employment.save();
  console.log("exited");
}

export async function AddPortfolio(body: any, _id: any): Promise<void> {
  console.log(body);
  let user = await get(User, _id);
  //@ts-ignore
  let portfolio: any = await get(Talent, user.talent);
  console.log(portfolio);
  await portfolio.portfolio.push(body);
  await portfolio.save();
  console.log("exited");
}

export async function DeletePortfolio(
  _id: Schema.Types.ObjectId,
  portID: Schema.Types.ObjectId
): Promise<void> {
  let talent: any = await get(Talent, _id);
  for (let i = 0; i < talent.portfolio.length; i++) {
    if (talent.portfolio[i]._id == portID) {
      await talent.portfolio.splice(i, 1);
    }
  }
  await talent.save();
}

export async function DeleteEducation(
  _id: Schema.Types.ObjectId,
  portID: Schema.Types.ObjectId
): Promise<void> {
  let talent: any = await get(Talent, _id);
  for (let i = 0; i < talent.education.length; i++) {
    if (talent.education[i]._id == portID) {
      await talent.education.splice(i, 1);
    }
  }
  await talent.save();
}

export async function DeleteEmployment(
  _id: Schema.Types.ObjectId,
  portID: Schema.Types.ObjectId
): Promise<void> {
  let talent: any = await get(Talent, _id);
  for (let i = 0; i < talent.employment.length; i++) {
    if (talent.employment[i]._id == portID) {
      await talent.employment.splice(i, 1);
    }
  }
  await talent.save();
}

export async function GetUserTalent(
  _id: Schema.Types.ObjectId
): Promise<any> {
  let user: any = await get(User, _id);
  let talent: any = await get(Talent, user.talent);
  talent.name = user.name;
  talent.phone = user.phone;
  talent.email = user.email;
  return { talent: talent, user: user };
}

export async function DeleteSkill(
  _id: Schema.Types.ObjectId,
  skillID: Schema.Types.ObjectId
): Promise<void> {
  let talent: any = await get(Talent, _id);
  for (let i = 0; i < talent.skills.length; i++) {
    console.log(talent.skills[i]._id);
    console.log(skillID);
    if (talent.skills[i]._id == skillID) {
      await talent.skills.splice(i, 1);
    }
  }
  await talent.save();
}

export async function EditSkill(
  _id: Schema.Types.ObjectId,
  skillID: Schema.Types.ObjectId,
  body: any
): Promise<void> {
  let talent: any = await get(Talent, _id);
  for (let i = 0; i < talent.skills.length; i++) {
    if (talent.skills[i]._id == skillID) {
      talent.skills[i] = body;
    }
  }
  await talent.save();
}

export async function EditPortfolio(
  _id: Schema.Types.ObjectId,
  portID: Schema.Types.ObjectId,
  body: any
): Promise<void> {
  let talent: any = await get(Talent, _id);
  for (let i = 0; i < talent.portfolio.length; i++) {
    if (talent.portfolio[i]._id == portID) {
      talent.portfolio[i] = body;
    }
  }
  await talent.save();
}
export async function EditEducation(
  _id: Schema.Types.ObjectId,
  educationID: Schema.Types.ObjectId,
  body: any
): Promise<void> {
  let talent: any = await get(Talent, _id);
  for (let i = 0; i < talent.education.length; i++) {
    if (talent.education[i]._id == educationID) {
      talent.education[i] = body;
    }
  }
  await talent.save();
}
export async function EditEmployment(
  _id: Schema.Types.ObjectId,
  empID: Schema.Types.ObjectId,
  body: any
): Promise<void> {
  let talent: any = await get(Talent, _id);
  for (let i = 0; i < talent.employment.length; i++) {
    if (talent.employment[i]._id == empID) {
      talent.employment[i] = body;
    }
  }
  await talent.save();
}

export async function RateUser(
  talentId: Schema.Types.ObjectId,
  body: any,
  userId: Schema.Types.ObjectId
): Promise<void> {
  let talent: ITalent = await get(Talent, talentId);
  body.from = userId;
  talent.review!.push(body);
  await talent.save();
}

export async function Search(term: string): Promise<any> {
    let talent: any = await Talent.find({
        $or: [
            {
                'skills.skill': term
            },
            {
                'education.field': term
            }
        ]
    });
    console.log(talent);
    if (talent.length > 0) {
        let user: any[]=[];
        for(let i=0;i<talent.length;i++){
          let u=await get(User,talent[i].userId);
          user.push(u)
        }

        return {user: user, talent: talent};
    } else {
        return []
    }
}
