import { Schema, Document } from "mongoose";
import { IMessage, Message } from "../models/message";
import { add, get, remove, list, edit } from "../services/crud";
import {User} from "../models/user";

export async function SendMessage(userId:Schema.Types.ObjectId,toId:Schema.Types.ObjectId,body:IMessage):Promise<void>{
    body.user=userId;
    body.to=toId;
    await  add(Message,body);
}

export async function GetMessage(_id: Schema.Types.ObjectId): Promise<any> {
    let message: any = await get(Message, _id);
    let user: any = await get(User, message.user);
    console.log(user);
    return {user: user, message: message}
}

export async function ListMessageConversation(_id:Schema.Types.ObjectId,userId:Schema.Types.ObjectId):Promise<IMessage[] | Document[]>{
    return await list(Message,{
        preQuery:model => model.find({user:userId })
    })
}
export async function ListAllMessage(_id:Schema.Types.ObjectId):Promise<any>{
    console.log("listing messages");
    let messages_user=await list(Message,{
        preQuery:model=>model.find({user:_id})
    });
    let messages_to=await list(Message,{
        preQuery:model=>model.find({to:_id})
    });
    let messages=[];
    messages.push(messages_to);
    messages.push(messages_user);
    return messages_to.concat(messages_user);
}
export async function EditMessage(_id:Schema.Types.ObjectId,body:IMessage):Promise<void>{
    // todo add authorization for editing the message
    await edit(Message,_id,body);
}

export async function DeleteMessage(messageId:Schema.Types.ObjectId,userId:Schema.Types.ObjectId):Promise<void>{
    let  message:any=await get(Message,messageId);
    if(message.user.toString() == userId.toString()){
        console.log("Removes the document");
        await remove(Message,messageId)
    }
    if(message.to.toString() == userId.toString()){
        console.log("Changes the to field to null");
        await edit(Message,messageId,{deleted:true})
    }
}