import * as KoaPassport from 'koa-passport'
import * as PassportLocal from 'passport-local'
import { Document } from 'mongoose'

import { KoaError } from '../services/koa-error'
import { User, IUser } from '../models/user';
// import { isPasswordCorrect } from '../services/password';
import {compareSync} from "bcrypt";

// account serialization and deserialization in/from context.state.user
KoaPassport.serializeUser((user: Document & IUser, done) => {
    done(null, user._id);
});

KoaPassport.deserializeUser((_id, done) => {
    User.findById(_id, (err, user) => done(err || null, user as any));
});

// configure local strategy
KoaPassport.use(
    'local',
    new PassportLocal.Strategy({ usernameField: 'email' }, (email, password, done) => {
        if (!email)
            return done(new KoaError('"email" parameter is not found.', 400, 'NO_EMAIL'))
        if (!password)
            return done(new KoaError('"password" parameter is not found.', 400, 'NO_PASSWORD'))

        User.findOne({email}, (err, user: any) => {
            if (err) return done(err)
            if (!user) return done(null, false);
            if (compareSync(password, user.password)) {
                done(null, user);
                console.log("Was here ");

            } else {
                done(err)
            }
            // check password
            /*compare(password, user.password).then(() => console.log(user.password))
            compare(password, user.password)
                .then((err) => {
                    if(err)
                        console.log(err)
                    else
                        done(null, user);
                    console.log("Was here ");
                })
                .catch((reason: String) => {console.log(reason);done(reason)})*/
        })
    })
)