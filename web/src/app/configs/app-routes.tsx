import React, { lazy } from 'react'
import { Route, Switch } from 'react-router-dom'
import { useAccountState } from '../stores/account/account-provider'
import { User } from "../../user/user";
import { Recruiter } from '../../recruiter/recruiter';
import { Login } from '../../shared/pages/login/login';
import { Register } from '../../shared/pages/register/register';
import { Landing } from '../../shared/pages/landing/landing';
const NotFound = lazy(() => import('../../shared/pages/not-found/not-found'))

export default function AppRoutes() {
  const { account } = useAccountState()

  return (
    <Switch>
      {account && account._UserProviderid ?
        <>
          {account.role === "Employee" ?
            <>
              <Route
                path={'/talent'}
                component={User}
              />
            </> :
            <Route
              path={'/recruiter'}
              component={Recruiter}
            />
          }
        </>
        :
        null
      }
      <Route exact path="/" component={Landing}/>
      <Route exact path="/login" component={Login} />
      <Route exact path="/register" component={Register}/>
      <Route component={NotFound} />
    </Switch>
  )
}
