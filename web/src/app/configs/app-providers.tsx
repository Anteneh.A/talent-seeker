import React, { PropsWithChildren } from 'react'
import { AccountProvider } from "../stores/account/account-provider";
import Layout from '../../shared/layout/layout';


export default function AppProviders({ children }: PropsWithChildren<{}>) {
  return (
    <Layout>
      <AccountProvider>
        {children}
      </AccountProvider>
    </Layout>
  )
}
