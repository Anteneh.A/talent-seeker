import React from 'react';
import './App.scss';
import AppRoutes from "./configs/app-routes";
import AppProviders from "./configs/app-providers";
import {BrowserRouter} from "react-router-dom";

function App() {
    return (
        <BrowserRouter>
            <AppProviders>
                <AppRoutes />
            </AppProviders>
        </BrowserRouter>
    )
}

export default App;
