import React, { useState } from "react";
import Rating from "@material-ui/lab/Rating";
import "./rating.scss";
import { Button, Modal, Paper, TextField } from "@material-ui/core";
import Axios from "axios";

interface IRatingProps {
  _id: string;
  rating: number;
  onClose: () => void;
  open: boolean;
}
export function Rate(props: IRatingProps) {
  let [rating, setRating] = useState(0);
  let [review, setReview] = useState();
  let reviewUser = function(e: any) {
    Axios.post(`/api/talent/${props._id}/review`, {
      review: review,
      rating: rating
    })
        .then(() => props.onClose())
      .catch();
  };
  return (
    <Modal open={props.open} onClose={() => props.onClose()}>
      <Paper className={"rate-form"}>
        <form
          onSubmit={e => {
            e.preventDefault();
            reviewUser(e);
          }}
        >
          <div className={"p-3 d-flex justify-content-around"}>
            <strong>Rate &emsp;</strong>
            <Rating
              name="simple-controlled"
              size={"medium"}
              value={rating}
              onChange={(e: object, value: number) => setRating(value)}
            />
          </div>
          <hr />
          <div className={"p-3"}>
            <TextField
              name={"review"}
              label={"Write a  Review"}
              fullWidth={true}
              multiline={true}
              onChange={e => setReview(e.target.value)}
              rows={3}
              required
            />
          </div>
          <div className={"p-3"}>
            <Button fullWidth={true} color={"primary"} type={"submit"} variant={"contained"}>
              Done
            </Button>
          </div>
        </form>
      </Paper>
    </Modal>
  );
}
