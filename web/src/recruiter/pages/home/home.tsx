import React, {useEffect, useState} from "react";
import "./home.scss";
import Axios from "axios";
import {UserCard} from "../../../shared/components/user-card/user-card";
import {LinearProgress} from "@material-ui/core";

export function Home() {
    let [user, setUser] = useState();
    let getSearchResult = function () {
        Axios.get("/api/user/all")
            .then(resp => setUser(resp.data))
            .catch();
    };
    useEffect(() => {
        getSearchResult();
    }, []);
    return user ? (
        <div className={"container home"}>
            <h2> Talent </h2>
            <div className="row">
                {user.user.map((account: any, index: number) => (
                    <>
                        {account.role === "Employee" ? (
                            <div className={"col-md-4 col-sm-6"}>
                                <UserCard user={user.user[index]} talent={user.talent[index]}/>
                            </div>
                        ) : (
                            ""
                        )}
                    </>
                ))}
            </div>
        </div>
    ) : (
        <LinearProgress/>
    );
}
