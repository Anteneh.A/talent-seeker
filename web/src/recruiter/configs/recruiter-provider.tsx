import React, { PropsWithChildren } from "react"

export default function Provider({ children }: PropsWithChildren<{}>) {
    return (
        <>
            {children}
        </>
    )
}