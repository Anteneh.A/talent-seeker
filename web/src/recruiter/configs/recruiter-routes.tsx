import React from "react";
import { Route, Switch } from "react-router";
import { Messages } from "../../shared/pages/messages/messages";
import { MessageDetail } from "../../shared/pages/message-detail/message-detail";
import Profile from "../../shared/pages/profile/profile";
import { Home } from "../pages/home/home";
import { Search } from "../../shared/pages/search/search";
export function RecruiterRoutes({ prefix: p, account }: { prefix: string, account: any }) {
    return (
        <Switch>
            <>
                <Route exact path={`${p}/`} component={Home} />
                <Route exact path={`${p}/messages`} component={Messages} />
                <Route exact path={`${p}/message/:_id`} component={MessageDetail} />
                <Route exact path={`${p}/account`} component={Profile} />
                <Route exact path={`${p}/search`} component={Search} />
            </>
        </Switch>
    )
}