import React from "react"
import Provider from "./configs/recruiter-provider";
import { RecruiterRoutes } from  "./configs/recruiter-routes";
import * as qs from 'qs'
import { RouteComponentProps } from "react-router";
import { useAccountState } from "../app/stores/account/account-provider";
import Layout from "../shared/layout/layout";

interface Props extends RouteComponentProps<{}> {
    error?: any 
}

export function Recruiter({ match }: Props) {
    const { account } = useAccountState()
    const q = qs.parse(window.location.search, { ignoreQueryPrefix: true })['no-shell']
    return (
        <Provider>
            <Layout>
                <RecruiterRoutes account={account} prefix={match.url.replace(/\/$/, '')}/>
            </Layout>
        </Provider>
    )
}