import React, { PropsWithChildren, ReactNode, Suspense, useEffect, useState } from 'react'
import { MinHeightProperty } from 'csstype'

import Header from './header/header'
import Footer from './footer/footer'
import {LinearProgress} from "@material-ui/core";

type ILayoutProps = PropsWithChildren<{
  error?: any
  nonContentHeight?: MinHeightProperty<string | number>
  noShell?: boolean
  overrideHeader?: ReactNode
  overrideFooter?: ReactNode
  preHeader?: ReactNode
}>

function Layout({
  children,
  nonContentHeight: nch,
  noShell: noShellProp,
  error,
  overrideHeader,
  overrideFooter,
  preHeader
}: ILayoutProps) {
  const [noShellState, setNoShellState] = useState(true)

  useEffect(() => {
    if (noShellProp === undefined) {
      const fromStorage = window.sessionStorage.getItem('noShell')
      setNoShellState(fromStorage === null ? false : fromStorage === 'true')
    } else {
      window.sessionStorage.setItem('noShell', String(noShellProp))
      setNoShellState(noShellProp)
    }
  }, [])

  const contentMinHeight = nch
    ? `calc(100vh - ${nch}${typeof nch === 'number' ? 'px' : ''})`
    : `100vh`

  return error ? (
    <div className={'container'}>
      <div className={"text-danger"}>{error}></div>
    </div>
  ) : (
    <>
      {!noShellState && (
        <>
          {preHeader}
          {overrideHeader || <Header />}
        </>
      )}

      <div style={{ minHeight: contentMinHeight }}>
        <Suspense fallback={<LinearProgress />}>{children}</Suspense>
      </div>

      {!noShellState && (overrideFooter || <Footer />)}
    </>
  )
}

export default Layout
