import React, { useEffect, useState } from "react";
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from "reactstrap";
import { Link, Redirect, Route } from "react-router-dom";
import { useAccountState } from "../../../app/stores/account/account-provider";
import Axios from "axios";
import SearchIcon from "@material-ui/icons/Search";
import { fade } from "@material-ui/core/styles";
import { InputBase, LinearProgress } from "@material-ui/core";

export default function Header() {
  let [search, setSearch] = useState();
  let [isOpen, setIsOpen] = useState();
  let [account, setAccount] = useState();
  
  let logout = function() {
    Axios.post("/api/user/logout")
      .then(() => {
        window.location.href = "/login";
      })
      .catch(e => {
        console.error(e);
      });
  };
  useEffect(() => {
    Axios.get("/api/user/me")
      .then(resp => setAccount(resp.data))
      .catch();
  }, []);
  return (
    <div className={"mb-5"}>
      <Navbar className={"fixed-top"} color="dark" dark expand="md">
        <NavbarBrand>
          {" "}
          <Link to={"/"} className={"nav-link text-white"}>
            Talent Seeker{" "}
          </Link>
        </NavbarBrand>
        <NavbarToggler onClick={() => setIsOpen(!isOpen)} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="ml-auto" navbar>
            {account ? (
              <>
                {account.role === "Employee" ? (
                  <>
                    <NavItem>
                      <Link
                        className={"nav-link"}
                        to={`/talent/${account._id}`}
                      >
                        Talent
                      </Link>
                    </NavItem>
                    <NavItem>
                      <Link className={"nav-link"} to="/">
                        Build-Talent
                      </Link>
                    </NavItem>
                    <NavItem>
                      <Link className={"nav-link"} to="/messages">
                        Messages
                      </Link>
                    </NavItem>
                    <UncontrolledDropdown nav inNavbar>
                      <DropdownToggle nav caret>
                        {account.name}
                      </DropdownToggle>
                      <DropdownMenu right>
                        <DropdownItem>
                          <Link to="/account">Account profile</Link>
                        </DropdownItem>
                        <DropdownItem onClick={() => logout()}>
                          Logout
                        </DropdownItem>
                        <DropdownItem divider />
                      </DropdownMenu>
                    </UncontrolledDropdown>
                  </>
                ) : (
                  <>
                    <NavItem>
                      <form onSubmit={e => {
                      }} action={"/search"} className="form-inline my-2 my-lg-0">
                        <input
                            className="form-control mr-sm-2"
                            name={"term"}
                            type="search"
                            placeholder="Search"
                            onChange={e => setSearch(e.target.value)}
                            aria-label="Search"
                            required
                        />
                        <button
                            className="btn btn-outline my-2 my-sm-0 text-white"
                            type="submit"
                        >
                          Search
                        </button>
                      </form>
                    </NavItem>
                    <NavItem>
                      <Link className={"nav-link"} to="/">
                        Home
                      </Link>
                    </NavItem>
                    <NavItem>
                      <Link className={"nav-link"} to="/messages">
                        Messages
                      </Link>
                    </NavItem>
                    <UncontrolledDropdown nav inNavbar>
                      <DropdownToggle nav caret>
                        {account.name}
                      </DropdownToggle>
                      <DropdownMenu right>
                        <DropdownItem>
                          <Link to="/account">Account profile</Link>
                        </DropdownItem>
                        <DropdownItem onClick={() => logout()}>
                          Logout
                        </DropdownItem>
                        <DropdownItem divider />
                      </DropdownMenu>
                    </UncontrolledDropdown>
                  </>
                )}
              </>
            ) : (
              <>
                <NavItem>
                  <form onSubmit={e => {}} action={"/search"} className="form-inline my-2 my-lg-0">
                    <input
                      className="form-control mr-sm-2"
                      name={"term"}
                      type="search"
                      placeholder="Search"
                      onChange={e => setSearch(e.target.value)}
                      aria-label="Search"
                      required
                    />
                    <button
                      className="btn btn-outline my-2 my-sm-0 text-white"
                      type="submit"
                    >
                      Search
                    </button>
                  </form>
                </NavItem>
                &emsp;
                <NavItem>
                  <Link className={"nav-link"} to="/login">
                    Login
                  </Link>
                </NavItem>
                <NavItem>
                  <Link className={"nav-link"} to="/register">
                    Register
                  </Link>
                </NavItem>
              </>
            )}
          </Nav>
        </Collapse>
      </Navbar>
    </div>
  );
}
