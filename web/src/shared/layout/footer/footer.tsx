import React from 'react'
// import { Anchor, Block, Flex, FlexSpacer } from 'gerami'

import './footer.scss'
import {Link} from "react-router-dom";

export default function Footer() {

  return (
      <div className={"Footer"}>
          <h5> Talent Seeker </h5>
        &copy; Copyright {new Date(Date.now()).getFullYear()}
      </div>
  )
}
