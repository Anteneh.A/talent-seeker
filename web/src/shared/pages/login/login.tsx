import React, { useState } from "react";

import Paper from "@material-ui/core/Paper";
import TextField from "@material-ui/core/TextField";
import { Button, Input, InputAdornment } from "@material-ui/core";
import "./login.scss";
import { Link } from "react-router-dom";
import { Mail, Lock } from "@material-ui/icons";
import Axios from "axios";

export function Login() {
  let [user, setUser] = useState({ email: "", password: "" });
  let login = function() {
    Axios.post("/api/user/login", { user })
      .then(() => alert("Logging in..."))
      .catch();
  };
  return (
    <div className={"container"}>
      <Paper className={"paper"} elevation={2}>
        <div
          style={{ display: new URLSearchParams(window.location.search).has("error")? "block" : "none" }}
          className="alert alert-danger"
          role="alert"
        >
          {new URLSearchParams(window.location.search).get("error")}
        </div>
        <form action={"/api/user/login"} method={"POST"}>
          <div className={"text-center"}>
            <h2>Login</h2>
          </div>
          <div className={"p-2"}>
            <TextField
              className={"txtfield"}
              name="email"
              label="E-mail"
              onChange={e => (user.email = e.target.value)}
              placeholder={"E-mail address..."}
              margin="normal"
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <Mail />
                  </InputAdornment>
                )
              }}
              required
            />
          </div>
          <div className="p-2">
            <TextField
              className={"txtfield"}
              label="Password"
              placeholder={"Password..."}
              name="password"
              type="password"
              margin="normal"
              onChange={e => (user.password = e.target.value)}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <Lock />
                  </InputAdornment>
                )
              }}
              required
            />
          </div>
          <div className={"d-flex justify-content-end"}>
            <Button variant="contained" type="submit" color="primary">
              Login
            </Button>
          </div>
        </form>
        <div className={"text-center"}>
          <Link to={"/register"}> Don't have an account? </Link>
        </div>
      </Paper>
    </div>
  );
}
