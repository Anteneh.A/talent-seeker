import React, {useState} from "react";
import "./landing.scss";
import Img from "../../../assets/images/landing.jpg";
import { UserCard } from "../../components/user-card/user-card";
import TSIcon from "@material-ui/icons/Business"
import WorkIcon from "@material-ui/icons/Work"
import AccountIcon from "@material-ui/icons/AccountCircle"
import CompareIcon from "@material-ui/icons/CompareArrows"
import HowReg from "@material-ui/icons/HowToReg"
import {Link} from "react-router-dom";
import Axios from "axios";

export function Landing() {
  let [user,setUsers]=useState();

  let getUsers=function(){
    Axios.get('/api/user/all')
        .then(resp=>setUsers(resp.data))
        .catch()
  }
  return (
    <div>
      <div className="masthead" style={{height:window.screen.height-90}}>
        <div className="container h-100">
          <div className="row h-100 align-items-center justify-content-center text-center">
              <div className="col-lg-10 align-self-end">
              <h1 className="text-uppercase text-white font-weight-bold">
                  <TSIcon style={{fontSize: "10.5rem"}}/><br/>
                  Welcome to Talent-Seeker
              </h1>
              <hr className="divider my-4" />
            </div>
            <div className="col-lg-8 align-self-baseline pb-3">
              <p className="text-white font-weight-light mb-5">
                Talent-Seeker lets you build up your profile and connects you with thousands of recruiters for free.
              </p>
              <Link className="btn btn-primary btn-xl js-scroll-trigger" to="/register">
                Get started
              </Link>
            </div>
          </div>
        </div>
      </div>
      <About />
      <Section/>
      <div className={"row"}>
        {/*{user.user.map((account: any, index: number) => (
            <>
              {account.role === "Employee" ? (
                  <div className={"col-md-4 col-sm-6"}>
                    <UserCard user={user.user[index]} talent={user.talent[index]}/>
                  </div>
              ) : (
                  ""
              )}
            </>
        ))}*/}
      </div>
    </div>
  );
}

function About() {
  return (
    <div className="page-section bg-primary p-5" id="about">
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-lg-8 text-center">
            <h2 className="text-white mt-0">We've got what you need!</h2>
            <hr className="divider light my-4" />
            <p className="text-white mb-4">
              A simpler way to find a job in what ever field of study you want.But in order to get your dream job you must register.
            </p>
            <Link
              className="btn btn-light btn-xl js-scroll-trigger"
              to={'/register'}
            >
              Get Started!
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
}

function Section() {
  return (
    <div className="page-section p-5" id="services">
      <div className="container">
        <h2 className="text-center mt-0">Our Goal</h2>
        <hr className="divider my-4" />
        <div className="row">
          <div className="col-lg-3 col-md-6 text-center">
            <div className="mt-5">
              <WorkIcon style={{fontSize:"5.5rem"}}/>
              <h3 className="h4 mb-2">Get you employed</h3>
              <p className="text-muted mb-0">
                Our web development team has created an amazing algorithm to get you a job
              </p>
            </div>
          </div>
          <div className="col-lg-3 col-md-6 text-center">
            <div className="mt-5">
              <AccountIcon style={{fontSize:"5.5rem"}}/>
              <h3 className="h4 mb-2">Build your portfolio</h3>
              <p className="text-muted mb-0">
                All information entered will be properly managed and stored on our servers
              </p>
            </div>
          </div>
          <div className="col-lg-3 col-md-6 text-center">
            <div className="mt-5">
              <CompareIcon style={{fontSize:"5.5rem"}}/>
              <h3 className="h4 mb-2">Create a connection</h3>
              <p className="text-muted mb-0">
                The information you enter will help recruiters to get contact you for their need
              </p>
            </div>
          </div>
          <div className="col-lg-3 col-md-6 text-center">
            <div className="mt-5">
              <HowReg style={{fontSize:"5.5rem"}}/>
              <h3 className="h4 mb-2">Simplify your life</h3>
              <p className="text-muted mb-0">
                Once you've set up everything,you don't need to worry about not having a job.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

