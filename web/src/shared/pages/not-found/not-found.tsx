import React from "react"
import "./not-found.scss"
import { Button, Fab } from "@material-ui/core";

import './not-found.scss';

export default function NotFound() {
    return (
        <div className="con">
            <h1 className="oops">
                Oops!
            </h1>
            <h3 className="page">404 - Page Not Found </h3>
            <p className="par">the page you are looking for might have been removed
           <span className="par"> had its name changed or is temporarily unavailable.</span></p>
            <Fab className="fab" variant="extended" color="primary">go to Homapage</Fab>
        </div>
    )
}