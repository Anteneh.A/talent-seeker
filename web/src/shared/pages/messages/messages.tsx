import React, { useState, useEffect } from "react";
import "./message.scss";
import { MessageTile } from "../../components/message-tile/message-tile";
import Axios from "axios";

import MessageIcon from "@material-ui/icons/Message";
import {ImagePicker} from "../../components/image-picker/image-picker";
interface IMessageProps {}

export function Messages(props: IMessageProps) {
  let [message, setMessage] = useState([]);
  let getMessages = function() {
    Axios.get("/api/message/list")
      .then(resp => {
        setMessage(resp.data);
        console.log(resp.data)
      })
      .catch();
  };
  useEffect(() => {
    getMessages();
  }, []);
  return message.length > 0 ? (
    <div className={"container pt-3"}>
      <h1 className={"p-4"}> Messages</h1>
      {message.map((mess: any) => (
        <MessageTile message={mess} />
      ))}
    </div>
  ) : (
    <div className={"p-5 m-5"}>
      <MessageIcon style={{ fontSize: "54px" }} color={"primary"} /> &emsp; No
      messages yet
    </div>
  );
}
