import React, { Fragment, useEffect, useState } from "react";
import "./search.scss";
import { UserCard } from "../../components/user-card/user-card";
import Axios from "axios";
import {
  List,
  ListItem,
  ListItemText,
  ListItemSecondaryAction
} from "@material-ui/core";
import Rating from "@material-ui/lab/Rating";
import {Link} from "react-router-dom";

const user = {
  name: "Anteneh Ashenafi",
  rating: 3.5,
  bio:
    "accomplished and energetic full stack developer with a solid history of achievment in developing web apps.",
  job: "Full-Stack Developer"
};

export function Search() {
  let [result, setResult] = useState();
  let getAverage = function (rating: any) {
    let sum = 0;
    for (let i = 0; i < rating.length; i++) {
      sum += rating[i].rating;
    }
    return sum / rating.length;
  };
  // let [user, setUser] = useState([]);
  // let getSearchResult = function() {
  //   Axios.get("/api/user/all")
  //     .then(resp => setUser(resp.data))
  //     .catch();
  // };

  // useEffect(() => {
  //   getSearchResult();
  // }, []);
  useEffect(() => {
    Axios.get(
      `/api/talent/search/${new URLSearchParams(window.location.search).get(
        "term"
      )}`
    )
      .then(resp=>setResult(resp.data))
      .catch();
  },[]);

  return result? (
    <div className={"search-result"}>
      {/*<div className="search">
        <input type="text" className="form-control" onChange={e => setQuery(e.target.value)} placeholder="ex. name, skill, field of study" />
        <button className="btn btn-default">Search</button>
      </div>*/}
      <div className=" container">
        {/*<h3>Search Results for "{} "</h3>*/}
        <div>
          {/*{user.map((account: any, index: number) => ( */}
          <List>
            {result.user.map((res:any,index:number)=> (
              <ListItem>
                <ListItemText
                  primary={<Link to={`/talent/${res._id}`} className={"nav-link"}>{res.name}</Link>}
                  secondary={result.talent[index].bio}
                />
                <ListItemSecondaryAction>
                  <Rating
                    name="simple-controlled"
                    value={result.talent[index].review.rating?getAverage(result.talent[index].review.rating):0}
                    readOnly={true}
                    size={"medium"}
                  />
                </ListItemSecondaryAction>
              </ListItem>
            ))}
          </List>
        </div>
      </div>
    </div>
  ) : (
    <div>
      <h3>No Result found</h3>
    </div>
  );
}
