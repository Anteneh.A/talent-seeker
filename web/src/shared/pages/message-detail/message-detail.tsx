import React, { useEffect, useState } from "react";
import "./message-detail.scss";
import { Button, LinearProgress, Modal, Paper, TextField } from "@material-ui/core";
import ReplyIcon from "@material-ui/icons/Reply";
import Axios from "axios";
import { Link } from "react-router-dom";

const data = {
  message:
    "  This is the message that will be displayed. This is the message that will be displayed. This is the message that will be displayed" +
    "This is the message that will be displayed. This is the message that will be displayed. This is the message that will be displayed" +
    "This is the message that will be displayed. This is the message that will be displayed",
  from: "Abebe Kebede",
  date: "Jan 2, 2019"
};

export function MessageDetail(props: any) {
  let [message, setMessage] = useState();
  let [replyForm, setReplyForm] = useState(false);
  let getMessageDetail = function () {
    Axios.get("/api/message/" + props.match.params._id)
      .then(resp => {
        console.log(resp.data)
        setMessage(resp.data)
      })
      .catch();
  };
  useEffect(() => {
    getMessageDetail();
  }, []);
  return message ? (
    <div className={"container message-detail"}>
      <div className={"card message-content"}>
        <div className={"card-body"}>
          <h5>{message.user.name}</h5>
          {/*  todo Change to senders name*/}
          <span className={"text-muted pl-3 message-date"}>{new Date(message.message.date).toDateString()} </span>
          <p className={"p-4 text-monospace text-muted"}>{message.message.message}</p>
        </div>
      </div>
      <div className={"p-3"}>
        <Link
          className="btn btn-light btn-xl js-scroll-trigger"
          to={'/messages'}
        >
          Go Back
        </Link>
        &emsp;
        <Button
          className={""}
          variant={"contained"}
          color={"primary"}
          onClick={() => setReplyForm(!replyForm)}
        >
          <ReplyIcon /> &nbsp;Reply{" "}
        </Button>
      </div>
      <MessageModal toId={message.message.user} open={replyForm} close={() => setReplyForm(!replyForm)} />
    </div>
  ) : <LinearProgress />
}

function MessageModal(props: {
  toId: string;
  open: boolean;
  close: () => void;
}) {
  let [message, setMessage] = useState({
    message: "",
    title: ""
  });
  let sendMessage = function () {
    Axios.post("/api/message/send/" + props.toId, message)
      .then(() => props.close())
      .catch();
  };
  return (
    <Modal open={props.open} onClose={() => props.close()}>
      <Paper className={"reply-form"}>
        <div className={"p-5"}>
          <div className={"text-center"}>
            <h5>Message</h5>
          </div>
          <form
            onSubmit={e => {
              e.preventDefault();
              sendMessage();
            }}
          >
            <div className={"reply-input"}>
              <TextField
                name={"title"}
                label={"Title"}
                onChange={e => (message.title = e.target.value)}
                fullWidth={true}
                required
              />
            </div>
            <div className={"reply-input"}>
              <TextField
                name={"message"}
                label={"Message"}
                onChange={e => message.message = e.target.value}
                multiline={true}
                rows={3}
                fullWidth={true}
                required
              />
            </div>
            <div className={"p-2 d-flex justify-content-end"}>
              <Button type={"submit"} variant={"contained"} color={"primary"}>
                Reply
                            </Button>
            </div>
          </form>
        </div>
      </Paper>
    </Modal>
  );
}

function ReplyModal(props: { open: boolean; close: () => void }) {
  return (
    <Modal open={props.open} onClose={() => props.close()}>
      <Paper className={"reply-form"}>
        <div className={"p-5"}>
          <form onSubmit={() => { }}>
            <div className={"reply-input"}>
              <TextField
                name={"reply"}
                label={"Reply"}
                multiline={true}
                rows={3}
                fullWidth={true}
                required
              />
            </div>
            <div className={"p-2 d-flex justify-content-end"}>
              <Button type={"submit"} variant={"contained"} color={"primary"}>
                Send
              </Button>
            </div>
          </form>
        </div>
      </Paper>
    </Modal>
  );
}
