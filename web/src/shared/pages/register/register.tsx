import React, { useState } from "react";
import "./register.scss";
import { Link } from "react-router-dom";
import {
  Button,
  InputAdornment,
  InputLabel,
  MenuItem,
  Select,
  TextField
} from "@material-ui/core";
import Paper from "@material-ui/core/Paper";
import Axios from "axios";
import { AccountCircle, Lock, Mail, Phone } from "@material-ui/icons";

export function Register() {
  let [user, setUser] = useState({
    name: "",
    email: "",
    role: "Employee",
    phone: "",
    password: "",
    confirmPassword: ""
  });
  let [error, setError] = useState();

  let register = function() {
    if (user.password === user.confirmPassword) {
      if (!/@moderneth.com\s*$/.test(user.email)) {
        console.log("it ends in @yahoo");
        setError("Email must be @moderneth.com");
      } else {
        Axios.post("/api/user/register", user)
          .then(() => {
            window.location.href = "/login";
          })
          .catch(console.error);
      }
    } else {
      setError("Passwords don't match");
    }
  };

  return (
    <div className={"container"}>
      <Paper className={"paper"} elevation={2}>
        <form
          className={"p-2"}
          onSubmit={e => {
            e.preventDefault();
            register();
          }}
        >
          <div className={"text-center"}>
            <h2> Register </h2>
          </div>
          <div
            style={{ display: error ? "block" : "none" }}
            className="alert alert-danger"
            role="alert"
          >
            {error ? error : ""}
          </div>
          <TextField
            className={"txtfield"}
            id="standard-required"
            name="name"
            required={true}
            label="Name"
            placeholder={"Full name.."}
            type={"text"}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <AccountCircle />
                </InputAdornment>
              )
            }}
            onChange={e => (user.name = e.target.value)}
            margin="normal"
          />
          <br />
          <TextField
            className={"txtfield"}
            id="standard-required"
            name="email"
            placeholder={"E-mail address.."}
            label="E-mail"
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <Mail />
                </InputAdornment>
              )
            }}
            margin="normal"
            onChange={e => (user.email = e.target.value)}
            required={true}
          />
          <br />
          <TextField
            className={"txtfield"}
            id="standard-required"
            name="phone"
            placeholder={"Phone number.."}
            label="Phone"
            required={true}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <Phone />
                </InputAdornment>
              )
            }}
            onChange={e => (user.phone = e.target.value)}
            margin="normal"
          />
          <br />
          <InputLabel htmlFor="Role">Role</InputLabel>
          <Select
            value={user.role}
            fullWidth={true}
            onChange={e => (user.role = e.target.value as string)}
            inputProps={{
              id: "Role"
            }}
            required
          >
            <MenuItem selected={true} value={"Employee"}>
              {" "}
              Employee{" "}
            </MenuItem>
            <MenuItem value={"Recruiter"}> Recruiter </MenuItem>
          </Select>
          <TextField
            className={"txtfield"}
            label="Password"
            placeholder={"Password"}
            name="password"
            type="password"
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <Lock />
                </InputAdornment>
              )
            }}
            onChange={e => (user.password = e.target.value)}
            required={true}
            margin="normal"
          />
          <br />
          <TextField
            className={"txtfield"}
            label="Confirm"
            placeholder="Confirm Password"
            name="confirmPassword"
            required={true}
            type="password"
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <Lock />
                </InputAdornment>
              )
            }}
            onChange={e => (user.confirmPassword = e.target.value)}
            margin="normal"
          />
          <br />
          <div className={"pt-1 d-flex justify-content-end"}>
            <Button
              variant="contained"
              //   onClick={this.onSubmit.bind(this)}
              type="submit"
              color="primary"
            >
              Register
            </Button>
          </div>
        </form>
        <div className={"pt-1 text-center"}>
          <Link to={"/login"}> Already have an account?</Link>
        </div>
      </Paper>
    </div>
  );
}
