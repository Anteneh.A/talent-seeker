import React, { Fragment, useEffect, useRef, useState } from "react";
import {
    Card,
    Grid,
    Avatar,
    Button,
    Paper,
    List,
    ListItemSecondaryAction,
    ListItem,
    ListItemText,
    IconButton,
    TextField,
    LinearProgress
} from "@material-ui/core";
import { Person, Edit, Done } from "@material-ui/icons";
import "./profile.scss";
import { useAccountState } from "../../../app/stores/account/account-provider";
import Axios from "axios";
import picture from "../../../assets/images/camera.svg";

function Profile() {
    let [account, setAccount] = useState();

    const [editName, setEditName] = useState(false);
    const [editEmail, setEditEmail] = useState(false);
    const [editPhone, setEditPhone] = useState(false);
    const [editPassword, setEditPassword] = useState(false);
    const [image, setImage] = useState(picture);
    let imagePicker = useRef<HTMLInputElement>(null);

    let updateProfile = function () {
        Axios.put(`/api/user/${account._id}`, { account })
            .then(() => {
            })
            .catch();
    };
    let getProfile = function () {
        Axios.get("/api/user/me")
            .then(resp => setAccount(resp.data))
            .catch();
    };
    let updatePassword = function () {
        Axios.put("/api/user/password", { account })
            .then(resp => {
            })
            .catch();
    };

    let changeImg = function (e: any, _id: any) {
        const file = e.target.files[0] as Blob;
        if (!file) {
            setImage(picture);
            return;
        }
        const reader = new FileReader();
        reader.onload = ev => setImage((ev.target as any).result);
        reader.readAsDataURL(file);
        console.log(file);
        let formData = new FormData()
        formData.append('picture', file);
        Axios.post(
            `/api/user/picture/${_id}`,
            formData,
            {
                headers: { "Content-Type": "multipart/form-data" },
                withCredentials: true
            }
        )
            .then()
            .catch();
    };

    let addPicture = function (file: any, _id: any) {
        Axios.post(`/api/user/picture/${_id}`, { picture: file })
            .then()
            .catch();
    };

    useEffect(() => {
        getProfile();
    }, []);

    return account ? (
        <Fragment>
            <Card elevation={2} className={"card p-4 account-profile"}>
                <Grid container>
                    <Grid className={"grid1"}>
                        <span>
                            <img
                                src={`/api/user/picture/${account._id}`}
                                width={60}
                                height={60}
                                onClick={() =>
                                    imagePicker.current && imagePicker.current.click()
                                }
                            />
                        </span>
                        <input
                            name={"picture"}
                            type={"file"}
                            ref={imagePicker}
                            id={"image-picker"}
                            style={{ display: "none" }}
                            onChange={e => changeImg(e, account._id)}
                        />
                    </Grid>
                    {!editName ? (
                        <div className={"d-flex justify-content-between"}>
                            <h2 className={"mt-3"}>{account.name}</h2>
                            <span className={"mt-3"}>
                                <Button
                                    color={"primary"}
                                    onClick={() => setEditName(!editName)}
                                >
                                    <Edit />
                                </Button>
                            </span>
                        </div>
                    ) : (
                            <div className="mt-3">
                                <TextField
                                    type={"text"}
                                    defaultValue={account.name}
                                    onChange={e => (account.name = e.target.value)}
                                />
                                <IconButton
                                    color="primary"
                                    onClick={() => {
                                        updateProfile();
                                        setEditName(!editName);
                                    }}
                                >
                                    <Done />
                                </IconButton>
                            </div>
                        )}
                </Grid>
                <hr />
                <div className="cont">
                    <List component="ul">
                        <ListItem>
                            {!editEmail ? (
                                <div>
                                    <ListItemText primary="Email" secondary={account.email} />
                                    <ListItemSecondaryAction>
                                        <IconButton
                                            color="primary"
                                            onClick={() => setEditEmail(!editEmail)}
                                        >
                                            <Edit />
                                        </IconButton>
                                    </ListItemSecondaryAction>
                                </div>
                            ) : (
                                    <div>
                                        <TextField
                                            className="txtfield"
                                            fullWidth={true}
                                            label={"E-mail"}
                                            defaultValue={account.email}
                                            onChange={e => (account!.email = e.target.value)}
                                            type={"email"}
                                            required
                                        />
                                        <ListItemSecondaryAction>
                                            <IconButton
                                                color="primary"
                                                onClick={() => {
                                                    updateProfile();
                                                    setEditEmail(!editEmail);
                                                }}
                                            >
                                                <Done />
                                            </IconButton>
                                        </ListItemSecondaryAction>
                                    </div>
                                )}
                        </ListItem>
                        <hr />

                        <ListItem>
                            {!editPhone ? (
                                <div>
                                    <ListItemText primary="Phone" secondary={account.phone} />
                                    <ListItemSecondaryAction>
                                        <IconButton
                                            color="primary"
                                            onClick={() => setEditPhone(!editPhone)}
                                        >
                                            <Edit />
                                        </IconButton>
                                    </ListItemSecondaryAction>
                                </div>
                            ) : (
                                    <div className={"fields"}>
                                        <TextField
                                            placeholder={"Phone"}
                                            label={"Phone Number "}
                                            name={"phone"}
                                            defaultValue={account.phone}
                                            onChange={e => (account.phone = e.target.value)}
                                            className={"full-width"}
                                            required
                                        />
                                        <ListItemSecondaryAction>
                                            <IconButton
                                                color="primary"
                                                onClick={() => {
                                                    setEditPhone(!editPhone);
                                                    updateProfile();
                                                }}
                                            >
                                                <Done />
                                            </IconButton>
                                        </ListItemSecondaryAction>
                                    </div>
                                )}
                        </ListItem>
                        <hr />
                        <ListItem>
                            {!editPassword ? (
                                <div>
                                    <ListItemText primary="Password" secondary="********" />
                                    <ListItemSecondaryAction>
                                        <IconButton
                                            color="primary"
                                            onClick={() => setEditPassword(!editPassword)}
                                        >
                                            <Edit />
                                        </IconButton>
                                    </ListItemSecondaryAction>
                                </div>
                            ) : (
                                    <div className={"fields"}>
                                        <TextField
                                            placeholder={"Password"}
                                            label={"Password "}
                                            name={"password"}
                                            defaultValue={"**********"}
                                            onChange={e => (account.password = e.target.value)}
                                            className={"full-width"}
                                            type={"password"}
                                            required
                                        />
                                        <ListItemSecondaryAction>
                                            <IconButton
                                                color="primary"
                                                onClick={() => {
                                                    updatePassword();
                                                    setEditPassword(!editPassword);
                                                }}
                                            >
                                                <Done />
                                            </IconButton>
                                        </ListItemSecondaryAction>
                                    </div>
                                )}
                        </ListItem>
                    </List>
                </div>
                {/*  */}

                {/*  */}
            </Card>
        </Fragment>
    ) : (
            <LinearProgress />
        );
}

export default Profile;
