import React, {useRef, useState} from "react";
import "./image-picker.scss";

export function ImagePicker() {
    const [picture, setPicture] = useState();
    const [imageSrc, setImageSrc] = useState();
    const pictureInputRef = useRef<HTMLInputElement>(null);
    const fileInputRef = useRef<HTMLInputElement>(null);

    let handlePictureInputChange = function () {
        if (
            pictureInputRef.current &&
            pictureInputRef.current.files &&
            pictureInputRef.current.files.length
        ) {
            console.log("");
            setPicture(pictureInputRef.current.files[0]);
            let reader = new FileReader();
            reader.onload = e => {
                setImageSrc((e.target as any).result);
            };
            reader.readAsDataURL(pictureInputRef.current.files[0]);
        }
    };
    return (
        <div>
      <span>
        <div
            className={"request-img-container"}
            style={{
                backgroundImage: `url(${imageSrc ? imageSrc : ""})`
            }}
            onClick={() =>
                pictureInputRef.current && pictureInputRef.current.click()
            }
        >
          <div style={{display: "none"}}>
            <input
                type={"file"}
                ref={pictureInputRef}
                onChange={() => handlePictureInputChange()}
            />
          </div>
          <div className={"img-add-placeholder"}>
            <span>Insert an image</span>
          </div>
        </div>
      </span>
        </div>
    );
}
