import React, { useState } from "react";
import "./user-card.scss";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import Rating from "@material-ui/lab/Rating";
import userImage from "../../../assets/images/user.jpg";
import { Message } from "@material-ui/icons";
import { Modal, Paper, TextField } from "@material-ui/core";
import Axios from "axios";
import {Link} from "react-router-dom";
const user = {
  name: "Anteneh Ashenafi",
  rating: 3.5,
  bio: "Some description about the user goes here",
  job: "Full-Stack Developer"
};

interface IUserProps {
  user: any;
  talent: any;
}
export function UserCard(props: IUserProps) {
  let [open, setOpen] = useState(false);
  let getAverage = function (rating: any) {
    let sum = 0;
    for (let i = 0; i < rating.length; i++) {
      sum += rating[i].rating;
    }
    return sum / rating.length;
  };
  return (
    <Card className={"m-3"}>
      <CardMedia
          image={`/api/user/picture/${props.user._id}` ? `/api/user/picture/${props.user._id}` : userImage}
          style={{ height: "0px", paddingTop: "50.00%" }}
      />
      <CardContent>
        <Typography>
          <strong><Link to={`/talent/${props.user._id}`}>{props.user.name}</Link></strong>
        </Typography>
        <Typography>
          <Rating
              name="simple-controlled"
              value={getAverage(props.talent.review)}
              readOnly={true}
              size={"small"}
          />
        </Typography>
        <Typography variant="body2" color="textSecondary" component="p">
          {props.talent.bio}
        </Typography>
      </CardContent>
      <CardActions>
        <div className={""}>
          <span className={"message-icon"}>
            <Button onClick={() => setOpen(true)} variant={"outlined"}>
              {/*<Message color={"primary"} fontSize={"small"} />*/}
              Message
            </Button>
          </span>
        </div>
      </CardActions>
      <MessageModal
        open={open}
        close={() => setOpen(!open)}
        toId={props.user._id}
      />
    </Card>
  );
}

function MessageModal(props: {
  toId: string;
  open: boolean;
  close: () => void;
}) {
  let [message, setMessage] = useState({
    message: "",
    title: ""
  });
  let sendMessage = function() {
    Axios.post("/api/message/send/" + props.toId, message)
      .then(() => props.close())
      .catch();
  };
  return (
    <Modal open={props.open} onClose={() => props.close()}>
      <Paper className={"reply-form"}>
        <div className={"p-5"}>
          <div className={"text-center"}>
              <h5>Message</h5>
          </div>
          <form
            onSubmit={e => {
              e.preventDefault();
              sendMessage();
            }}
          >
            <div className={"reply-input"}>
              <TextField
                name={"title"}
                label={"Title"}
                onChange={e => (message.title = e.target.value)}
                fullWidth={true}
                required
              />
            </div>
            <div className={"reply-input"}>
              <TextField
                name={"message"}
                label={"Message"}
                onChange={e => message.message=e.target.value}
                multiline={true}
                rows={3}
                fullWidth={true}
                required
              />
            </div>
            <div className={"p-2 d-flex justify-content-end"}>
              <Button type={"submit"} variant={"contained"} color={"primary"}>
                Send
              </Button>
            </div>
          </form>
        </div>
      </Paper>
    </Modal>
  );
}
