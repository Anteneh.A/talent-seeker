import React from 'react'
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Typography from '@material-ui/core/Typography';
import MailIcon from "@material-ui/icons/Mail"
import "./message-tile.scss"
import {Link} from "react-router-dom";
import {useAccountState} from "../../../app/stores/account/account-provider";

interface IMessageTileProps {
    message:{
        _id:string,
        date:Date,
        message:string,
        user:string,
        title:string,
        to:string,
        seen:boolean,
        deleted:boolean
    }
}

export function MessageTile(props: IMessageTileProps) {
    let {account}=useAccountState()

    return props.message && (
        <List className={"list-tile"}>
            <ListItem alignItems="flex-start">
                <ListItemAvatar className={"p-2"}>
                    <MailIcon color={"primary"}/>
                </ListItemAvatar>
                <ListItemText
                    primary={<Link to={`/message/${props.message._id}`} className={props.message.seen?"text-muted":""}> {props.message.title} </Link>}
                    secondary={
                        <React.Fragment>
                            <Typography
                                component="span"
                                variant="body2"
                                color="textPrimary"
                            >
                                {account.name}&emsp;
                            {/*    todo change this to senders name*/}
                            </Typography>
                            {props.message.message}....
                        </React.Fragment>
                    }
                />
            </ListItem>
            <Divider variant="inset" component="li"/>
        </List>
    )
}