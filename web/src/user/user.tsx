import React from "react"
import Provider from "./configs/user-provider";
import { UserRoutes } from "./configs/user-routes";
import * as qs from 'qs'
import { RouteComponentProps } from "react-router";
import { useAccountState } from "../app/stores/account/account-provider";
import Layout from "../shared/layout/layout";

interface Props extends RouteComponentProps<{}> {
    error?: any 
}

export function User({ match }: Props) {
    const { account } = useAccountState()
    const q = qs.parse(window.location.search, { ignoreQueryPrefix: true })['no-shell']
    return (
        <Provider>
            <Layout>
                <UserRoutes account={account} prefix={match.url.replace(/\/$/, '')}/>
            </Layout>
        </Provider>
    )
}