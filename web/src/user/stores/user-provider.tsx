import React from "react"
import { useReducer, useEffect, Dispatch, useContext } from 'react'
import { Action, reducer, state, State } from "../stores/user-reducer";

export const UserContextState = React.createContext<State>(state)
export const UserContextDispatch = React.createContext<Dispatch<Action>>(() => { })

export function UserProvider({ children }: { children: React.ReactNode }) {
    let [UserState, dispatch] = useReducer(reducer, state)

    return (
        <UserContextState.Provider value={UserState}>
            <UserContextDispatch.Provider value={dispatch}>
                {children}
            </UserContextDispatch.Provider>
        </UserContextState.Provider>
    )
}

export function useUserState() {
    return useContext(UserContextState)
}

export function useAdminDispatch() {
    return useContext(UserContextDispatch)
}