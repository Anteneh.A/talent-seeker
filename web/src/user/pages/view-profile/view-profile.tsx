import React, { useEffect, useState } from "react";
import "./view-profile.scss";
import { Button, Card, LinearProgress } from "@material-ui/core";
import SchoolIcon from "@material-ui/icons/School";
import EmailIcon from "@material-ui/icons/Email";
import PhoneIcon from "@material-ui/icons/LocalPhone";
import Axios from "axios";
import userImage from "../../../assets/images/user.jpg"
import { useAccountState } from "../../../app/stores/account/account-provider";
import { Rating } from "@material-ui/lab";
import { Rate } from "../../../recruiter/components/rating/rating";

interface IRating {
  rating: { rating: number; review: string }[];
}

export function ViewProfile(props: any) {
  let [open, setOpen] = useState(false);
  let [profile, setProfile] = useState<any>();
  let { account } = useAccountState();
  let getProfile = function () {
    Axios.get(`/api/talent/user/${props.match.params._id}`)
      .then(resp => setProfile(resp.data))
      .catch(console.error);
  };

  let getAverage = function (rating: any) {
    let sum = 0;
    for (let i = 0; i < rating.length; i++) {
      sum += rating[i].rating;
    }
    return sum / rating.length;
  };
  useEffect(() => {
    getProfile();
  }, []);

  return profile ? (
    <div className={"container pt-3"}>
      {/*todo view users talent and skill for both recruiter and user*/}
      <Card className={"m-5 p-4"}>
        <div className={"row"}>
          <div className={"col-md-6 col-sm-12 pl-5 imageContainer"}>
            <img
                src={`/api/user/picture/${profile.user._id}` ? `/api/user/picture/${profile.user._id}` : userImage}
                className={"userImage img-thumbnail"}
            />
          </div>
          <div className={"col-md-6 col-sm-12 nameContainer"}>
            <span className={"userName"}> {profile.user.name} </span>
            <div>
              <span className={"jobTitle pl-4 pb-2"}> &nbsp;Developer </span>
            </div>
            <div className={"row pl-5"}>
              <EmailIcon style={{ fontSize: "16px" }} />
              &nbsp;<span className={"email"}>{profile.user.email}</span>
            </div>
            <div className={"row pl-5"}>
              <PhoneIcon style={{ fontSize: "16px" }} />
              &nbsp;<span className={"email"}>{profile.user.phone}</span>
            </div>
            <div>
              &emsp;
              <p className={"bioText"}>{profile.talent.bio}</p>
            </div>
          </div>
        </div>
        <div className={"row pl-5"}>
          <div className={"col-md-12 col-sm-12"}>
            {profile.user.role !== "Recruiter" ? (
              <Rating
                size={"small"}
                readOnly={false}
                value={getAverage(profile.talent!.review)}
                onClick={() => setOpen(true)}
              />
            ) : (
                <Rating
                  size={"small"}
                  readOnly={true}
                  value={getAverage(profile.talent!.review)}
                />
              )}
            <Rate
              _id={profile.user.talent}
              rating={5}
              onClose={() => setOpen(false)}
              open={open}
            />
              &emsp;
              {profile.talent.availability.available ? (
                  <strong> Available for hire </strong>
              ) : (
                  <strong>Employed</strong>
              )}
          </div>
        </div>
        <div className={"row"}>
          <div className={"col-md-6 col-sm-12 p-5 educationContainer"}>
            <h3 className={"talentTitle"}> Education </h3>
            <hr />
            <div>
              {profile.talent.education.length > 0 ? (
                <span>
                  {profile.talent.education.map((education: any) => (
                    <div className={"pl-1 pt-2"}>
                      <div className={"schoolName"}>
                        {" "}
                        {education.field} (
                        <span className={"educationDate"}>
                          {" "}
                          {new Date(education.startDate).toDateString()} to{" "}
                          {new Date(education.endDate).toDateString()}{" "}
                        </span>
                        )
                      </div>
                      <div>
                        <SchoolIcon
                          style={{ fontSize: "18px" }}
                          className={""}
                        />{" "}
                        &emsp;{education.school}
                      </div>
                    </div>
                  ))}
                </span>
              ) : (
                  <h5>None</h5>
                )}
            </div>
          </div>
          <div className={"col-md-6 col-sm-12 p-5 educationContainer"}>
            <h3 className={"talentTitle"}> Work experience</h3>
            <hr />
            <div>
              {profile.talent.employment.length > 0 ? (
                <span>
                  {profile.talent.employment.map((employment: any) => (
                    <div className={"companyDescription pt-2 pl-1"}>
                      <div className={"employmentDate"}>
                        {" "}
                        {new Date(employment.startDate).toDateString()} -{" "}
                        {new Date(employment.endDate).toDateString()}{" "}
                      </div>
                      <div className={"companyTitle pl-2"}>
                        {" "}
                        {employment.company} (
                        <span className={"jobTitle"}>
                          {" "}
                          {employment.jobTitle}{" "}
                        </span>
                        )
                      </div>
                      <div className={"pl-3"}>
                        <p className={"employmentDescription"}>
                          {employment.description}
                        </p>
                      </div>
                    </div>
                  ))}
                </span>
              ) : (
                  <h5>None</h5>
                )}
            </div>
          </div>
        </div>

        <div className={"row"}>
          <div className={"col-md-6 col-sm-12 pl-5 educationContainer"}>
            <h3 className={"talentTitle"}> Skills </h3>
            <hr />
            {profile.talent.skills.length > 0 ? (
              <span>
                {profile.talent.skills.map((skill: any) => (
                  <div>
                    <ul>
                      <li>
                        <div className={"list d-flex justify-content-between"}>
                          <span className={"skill"}>{skill.skill}</span>
                          <span className={"skillLevel"}>{skill.level}</span>
                        </div>
                      </li>
                    </ul>
                  </div>
                ))}
              </span>
            ) : (
                <h5>None</h5>
              )}
          </div>
          <div className={"col-md-6 col-sm-12 pl-5 educationContainer"}>
            <h3 className={"talentTitle"}> Portfolio (Projects)</h3>
            <hr />
            {profile.talent.portfolio.length > 0 ? (
              <span>
                {profile.talent.portfolio.map((port: any) => (
                  <>
                    <div className={"projectName"}>
                      {" "}
                      {port.link ? (
                        <a target={"_blank"} href={port.link}>
                          {" "}
                          {port.project}{" "}
                        </a>
                      ) : (
                          port.project
                        )}
                    </div>
                    <p className={"pl-2 projectDescription"}>
                      {port.description}
                    </p>
                  </>
                ))}
              </span>
            ) : (
                <h5>None</h5>
              )}
          </div>
        </div>
      </Card>
    </div>
  ) : (
      <LinearProgress />
    );
}
