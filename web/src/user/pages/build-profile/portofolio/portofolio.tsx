import React, {useEffect, useState} from "react";
import {Button, Card, LinearProgress, TextField} from "@material-ui/core";
import Edit from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import Axios from "axios";

interface IPortfolioProps {
  talentId: string;
  portfolio: [
    {
      project: string;
      description: string;
      link: string;
    }
  ];
}

export function Portfolio(props: IPortfolioProps) {
  let [portofolioForm, setPortofolioForm] = useState(false);
  let [portfolio, setPortfolio] = useState({
    project: "",
    description: "",
    link: ""
  });
    let [resp, setResp] = useState();
  let add = function() {
    Axios.put(
      "/api/talent/portfolio/add",
      JSON.parse(JSON.stringify(portfolio))
    )
        .then(() => {
            getProfile();
            setPortofolioForm(false);
        })
      .catch();
  };
    let getProfile = function () {
        Axios.get("/api/talent/mine")
            .then(resp => {
                setResp(resp.data);
            })
            .catch(console.error);
    };

    useEffect(() => {
        getProfile();
    }, []);
    return resp ? (
    <Card className={"skill-form"}>
      <span className={"card-title"}> #Portofolio</span>
      <div className={"Fields"}>
        <span className={"d-flex justify-content-between"}>
          <span className={"title"}> Projects </span>
          <span
            style={{ display: !portofolioForm ? "block" : "none" }}
            className={"addButton"}
            onClick={() => setPortofolioForm(!portofolioForm)}
          >
            +
          </span>
        </span>
        <hr />
        <div className={"block "}>
          <div>
            <div>
                {resp.portfolio.length > 0 ? (
                <div>
                    {resp.portfolio.map((portfolio: any) => (
                    <PortfolioTile
                        update={() => getProfile()}
                        talentId={props.talentId}
                        _id={portfolio._id}
                        description={portfolio.description}
                        link={portfolio.link}
                        project={portfolio.project}
                    />
                  ))}
                </div>
              ) : (
                <h6
                  className={"m-2"}
                  style={{ display: !portofolioForm ? "block" : "none" }}
                >
                  {" "}
                  None{" "}
                </h6>
              )}
            </div>
            <div style={{ display: portofolioForm ? "block" : "none" }}>
              <form
                onSubmit={e => {
                  e.preventDefault();
                  add();
                }}
              >
                <div className={"Fields"}>
                  <TextField
                      name={"project"}
                      placeholder={"Add your Project"}
                      label={"Project"}
                      type={"text"}
                      fullWidth={true}
                      onChange={e => (portfolio.project = e.target.value)}
                      required
                  />
                </div>

                <div className={"Fields"}>
                  <TextField
                      name={"Description"}
                      type={"text"}
                      placeholder={"Description..."}
                      label={"Description"}
                      onChange={e => (portfolio.description = e.target.value)}
                      fullWidth={true}
                      required
                  />
                </div>

                <div className={"Fields"}>
                  <TextField
                    name={"link"}
                    placeholder={"Project link..."}
                    label={"Link"}
                    onChange={e => (portfolio.link = e.target.value)}
                    fullWidth={true}
                    required
                  />
                </div>

                <div className={"Fields d-flex justify-content-end"}>
                  <Button
                    variant="contained"
                    color={"primary"}
                    type={"submit"}
                    className={"primary "}
                    onClick={() => {}}
                  >
                    {" "}
                    Add{" "}
                  </Button>
                </div>

                <div className={""}>
                  <Button
                    color={"secondary"}
                    onClick={() => setPortofolioForm(!portofolioForm)}
                  >
                    Cancel
                  </Button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </Card>
    ) : (
        <LinearProgress/>
    );
}

function EditPortfolio(props: {
  _id: string;
  talentId: string;
  project: string;
  description: string;
  link: string;
  editPortfolio: () => void;
    update: () => void;
}) {
    let [portfolio, setPortfolio] = useState({
        project: props.project,
        description: props.description,
        link: props.link
    });
    let edit = function () {
        Axios.put(
            `/api/talent/${props.talentId}/portfolio/${props._id}`,
            JSON.parse(JSON.stringify(portfolio))
        )
            .then(() => {
                props.editPortfolio();
                props.update();
            })
            .catch();
    };
  return (
      <form
          onSubmit={e => {
              e.preventDefault();
              edit();
          }}
      >
      <div className={"Fields"}>
        <TextField
            name={"project"}
            placeholder={"Add your Project"}
            label={"Project"}
            defaultValue={props.project}
            fullWidth={true}
            onChange={e => (portfolio.project = e.target.value)}
            required
        />
      </div>

      <div className={"Fields"}>
        <TextField
            name={"Description"}
            placeholder={"Description..."}
            label={"Description"}
            fullWidth={true}
            onChange={e => (portfolio.description = e.target.value)}
            defaultValue={props.description}
            required
        />
      </div>

      <div className={"Fields"}>
        <TextField
            name={"link"}
            placeholder={"Project link..."}
            defaultValue={props.link}
            onChange={e => (portfolio.link = e.target.value)}
            label={"Link"}
            fullWidth={true}
        />
      </div>

      <div className={"Fields d-flex justify-content-end"}>
        <Button
          variant="contained"
          color={"primary"}
          type={"submit"}
          className={"primary "}
          onClick={() => {}}
        >
          {" "}
          Update{" "}
        </Button>
      </div>

      <div className={""}>
        <Button color={"secondary"} onClick={() => props.editPortfolio()}>
          Cancel
        </Button>
      </div>
    </form>
  );
}

function PortfolioTile(props: {
  _id: string;
  project: string;
  description: string;
  link: string;
  talentId: string;
    update: () => void;
}) {
  let [editPortofolio, setEditPortofolio] = useState(false);
    let deleteProject = function () {
        Axios.delete(`/api/talent/${props.talentId}/portfolio/${props._id}`)
            .then(() => {
                setEditPortofolio(false);
                props.update();
            })
            .catch();
    };
  return (
    <>
      {editPortofolio ? (
        <EditPortfolio
            _id={props._id}
            talentId={props.talentId}
            project={props.project}
            description={props.description}
            update={() => props.update()}
            link={props.link}
            editPortfolio={() => setEditPortofolio(!editPortofolio)}
        />
      ) : (
        <div className={"d-flex justify-content-between"}>
          <span className={""}>
            <div className={"projectName"}>
              {" "}
              {props.link ? (
                <a target={"_blank"} href={props.link}>
                  {" "}
                  {props.project}{" "}
                </a>
              ) : (
                props.project
              )}
            </div>
            <p className={"pl-2 projectDescription"}>
              {props.description.substr(0, 50)}...
            </p>
          </span>
          <span>
            <Edit
              color={"primary"}
              className={"pointer"}
              onClick={() => setEditPortofolio(true)}
            />
            &nbsp;
              <DeleteIcon
                  className={"trash-icon pointer"}
                  onClick={() => deleteProject()}
              />
          </span>
        </div>
      )}
    </>
  );
}
