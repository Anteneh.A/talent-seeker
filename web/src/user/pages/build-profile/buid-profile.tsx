import React, { useEffect, useState } from "react";
import "./build-profile.scss";
import { Education } from "./education/education";
import { Portfolio } from "./portofolio/portofolio";
import { Employment } from "./employment/employment";
import { Skills } from "./skill/skill";
import { LinearProgress, Switch, TextField } from "@material-ui/core";
import DoneIcon from "@material-ui/icons/Done";
import EditIcon from "@material-ui/icons/Edit";
import Axios from "axios";
import { useAccountState } from "../../../app/stores/account/account-provider";

const data = {
  bio: "Some information about the user goes here",
  available: true
};

interface ITalent {
  userId: string;
  _id: string;
  bio?: String;
  skills: [{ skill: String; level: String }];
  portfolio?: [
    {
      project: String;
      description: String;
      link: String;
    }
  ];
  employment?: [
    {
      company: { type: String; required: true };
      startDate: Date;
      endDate: Date;
      jobTitle: String;
      description: String;
    }
  ];
  education?: [
    {
      school: String;
      field: String;
      startDate: Date;
      endDate: Date;
    }
  ];
  availability?: {
    available: Boolean;
    availabilityDate: Date;
  };
  review?: [
    {
      _at: Date;
      rating: Number;
      review: String;
      from: string;
    }
  ];
}

export function BuildProfile() {
  let { account } = useAccountState();
  let [profile, setProfile] = useState();
  let [editBio, setEditBio] = useState(false);
  let getProfile = function() {
    Axios.get("/api/talent/mine")
      .then((res: any) => {
        setProfile(res.data);
      })
      .catch(console.error);
  };
    let edit = function (_id: string) {
        Axios.put(`/api/talent/${_id}`, profile)
            .then(() => {
                setEditBio(false);
                getProfile();
            })
            .catch();
  };
    let toggleAvailable = function (_id: string, value: boolean) {
        profile.availability.available = value;
        edit(_id);
    };
  useEffect(() => {
    getProfile();
  }, []);

  return profile ? (
    <div className={"container pt-3"}>
      <div className="d-flex justify-content-between m-5">
        <span>
          <h1> {account.name} </h1>
          <p className={"text-muted"}>
            Completely filling out the forms below will help recruiters to find
            you easily.
          </p>
        </span>
        <span className={"pt-3"}>
          <strong> Available </strong> &emsp;
            <Switch
                name={"availability"}
                checked={profile.availability.available}
                onChange={e => toggleAvailable(profile._id, e.target.checked)}
            />
        </span>
      </div>
      <div className={"m-5"}>
        <form>
          <div className={"d-flex justify-content-between"}>
            {editBio ? (
              <form className={"bio d-flex justify-content-between"}>
                <div className={"full-width"}>
                  <TextField
                      multiline={true}
                      name={"bio"}
                      defaultValue={profile.bio}
                      fullWidth={true}
                      placeholder={"Add your bio"}
                      onChange={e => (profile.bio = e.target.value)}
                      label={"Bio"}
                      rows={3}
                      required
                  />
                </div>
                <div className={"doneButton justify-content-end"}>
                    <DoneIcon
                        color={"primary"}
                        onClick={() => edit(profile._id)}
                    />
                </div>
              </form>
            ) : (
              <div className={"bio d-flex justify-content-between"}>
                <div className={"full-width"}>
                  <p className="muted"> {profile.bio} </p>
                </div>
                <div className={"justify-content-end"}>
                  <EditIcon
                    color={"primary"}
                    onClick={() => setEditBio(!editBio)}
                  />
                </div>
              </div>
            )}
          </div>
        </form>
      </div>
      <div className={"row"}>
        <div className={"col-md-6 col-sm-12"}>
          <Skills talentId={profile._id} skill={profile!.skills} />
        </div>

        <div className={"col-md-6  col-sm-12"}>
          <Employment talentId={profile._id} employment={profile.employment} />
        </div>
      </div>
      <div className={"row"}>
        <div className={"col-md-6 col-sm-12"}>
          <Portfolio talentId={profile._id} portfolio={profile.portfolio} />
        </div>

        <div className={"col-md-6 col-sm-12"}>
          <Education talentId={profile._id} education={profile.education} />
        </div>
      </div>
    </div>
  ) : (
    <div>
      <LinearProgress />
    </div>
  );
}
