import React, {useEffect, useState} from "react";
import {Button, Card, LinearProgress, TextField} from "@material-ui/core";
import "./education.scss";
import SchoolIcon from "@material-ui/icons/School";
import Edit from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import Axios from "axios";

interface IEducation {
  talentId: string;
  education: [
    {
      school: string;
      field: string;
      startDate: Date;
      endDate: Date;
    }
  ];
}

const data = [
  {
    school: "Fountain of Knowledge School",
    field: "Elementary",
    startDate: "January 1998 ",
    endDate: "June 2015"
  },
  {
    school: "Addis Ababa Science and technology University",
    field: "Computer Science and Information technology",
    startDate: "January 2019",
    endDate: "June 2019"
  }
];

export function Education(props: IEducation) {
  let [resp, setResp] = useState();
  let [educationForm, setEducationForm] = useState(false);
  let [education, setEducation] = useState({
    school: "",
    startDate: "",
    endDate: "",
    field: ""
  });
  let add = function() {
    Axios.put(
      "/api/talent/education/add",
      JSON.parse(JSON.stringify(education))
    )
        .then(() => {
          setEducationForm(false);
          getProfile()
        })
      .catch();
  };
  let getProfile = function () {
    Axios.get("/api/talent/mine")
        .then(resp => {
          setResp(resp.data);
        })
        .catch(console.error);
  };

  useEffect(() => {
    getProfile();
  }, []);
  return resp ? (
    <Card className={"skill-form"}>
      <span className={"card-title"}> #Education</span>
      <div className={"Fields"}>
        <span className={"d-flex justify-content-between"}>
          <span className={"title"}> Education background</span>
          <hr />
          <span
            style={{ display: !educationForm ? "block" : "none" }}
            className={"addButton"}
            onClick={() => setEducationForm(!educationForm)}
          >
            {" "}
            +{" "}
          </span>
        </span>
        <hr />
        <div className={"block "}>
          <div>
            <div>
              {resp.education.length > 0 ? (
                <div>
                  {resp.education.map((education: any) => (
                    <EducationTile
                        talentId={props.talentId}
                        _id={education._id}
                        school={education.school}
                        endDate={education.startDate}
                        field={education.field}
                        update={() => getProfile()}
                        startDate={education.startDate}
                    />
                  ))}
                </div>
              ) : (
                <h6
                  className={"m-2"}
                  style={{ display: !educationForm ? "block" : "none" }}
                >
                  {" "}
                  None{" "}
                </h6>
              )}
            </div>
            <div style={{ display: educationForm ? "block" : "none" }}>
              <form
                onSubmit={e => {
                  e.preventDefault();
                  add();
                }}
              >
                <div className={"Fields"}>
                  <TextField
                    name={"school"}
                    placeholder={"School"}
                    label={"School"}
                    onChange={e => (education.school = e.target.value)}
                    fullWidth={true}
                    required
                  />
                </div>

                <div className={"Fields"}>
                  <TextField
                    name={"field"}
                    placeholder={"Field of study..."}
                    label={"Field"}
                    onChange={e => (education.field = e.target.value)}
                    fullWidth={true}
                    required
                  />
                </div>

                <div className={"pl-2 pt-1 row "}>
                  <div className={"col-md-6"}>
                    <TextField
                      name={"startDate"}
                      label={"Start date"}
                      type={"date"}
                      onChange={e => (education.startDate = e.target.value)}
                      defaultValue={Date.now()}
                      fullWidth={true}
                      required
                    />
                  </div>
                  <div className={"col-md-6"}>
                    <TextField
                      name={"endDate"}
                      label={"End date"}
                      onChange={e => (education.endDate = e.target.value)}
                      type={"date"}
                      defaultValue={Date.now()}
                      fullWidth={true}
                      required
                    />
                  </div>
                </div>

                <div className={"Fields d-flex justify-content-end"}>
                  <Button
                    variant="contained"
                    color={"primary"}
                    type={"submit"}
                    className={"primary "}
                  >
                    {" "}
                    Add{" "}
                  </Button>
                </div>

                <div className={""}>
                  <Button
                    color={"secondary"}
                    onClick={() => setEducationForm(!educationForm)}
                  >
                    Cancel
                  </Button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </Card>
  ) : (
      <LinearProgress/>
  );
}

function EducationTile(props: {
  _id: string;
  talentId: string;
  school: string;
  startDate: string;
  endDate: string;
  update: () => void;
  field: string;
}) {
  let [editEducation, setEditEducation] = useState(false);
  let deleteEducation = function() {
    Axios.delete(`/api/talent/${props.talentId}/education/${props._id}`)
        .then(() => {
          setEditEducation(false);
          props.update();
        })
      .catch();
  };
  return (
    <>
      {editEducation ? (
        <EditEducation
            _id={props._id}
            talentId={props.talentId}
            school={props.school}
            endDate={props.startDate}
            field={props.field}
            update={() => props.update()}
            startDate={props.startDate}
            editEducation={() => setEditEducation(!editEducation)}
        />
      ) : (
        <div className={"pl-1 pt-2 d-flex justify-content-between"}>
          <span>
            <div className={"field-name"}>
              {" "}
              {props.field} (
              <span className={"educationDate"}>
                {" "}
                {new Date(props.startDate).toDateString()} to{" "}
                {new Date(props.endDate).toDateString()}{" "}
              </span>
              )
            </div>
            <div className={"school-name"}>{props.school}</div>
          </span>
          <span>
            <Edit
              color={"primary"}
              className={"pointer"}
              onClick={() => setEditEducation(true)}
            />
            &nbsp;
            <DeleteIcon
              className={"trash-icon pointer"}
              onClick={() => deleteEducation()}
            />
          </span>
        </div>
      )}
    </>
  );
}

function EditEducation(props: {
  _id: string;
  talentId: string;
  school: string;
  update: () => void;
  startDate: string;
  endDate: string;
  field: string;
  editEducation: () => void;
}) {
  let [education, setEducation] = useState({
    school: props.school,
    startDate: props.startDate,
    endDate: props.endDate,
    field: props.field
  });
  let edit = function() {
    Axios.put(
      `/api/talent/${props.talentId}/education/${props._id}`,
      JSON.parse(JSON.stringify(education))
    )
        .then(() => {
          props.editEducation();
          props.update();
        })
      .catch();
  };

  return (
    <form
      onSubmit={e => {
        e.preventDefault();
        edit();
      }}
    >
      <div className={"Fields"}>
        <TextField
          name={"school"}
          placeholder={"School"}
          label={"School"}
          defaultValue={props.school}
          onChange={e => (education.school = e.target.value)}
          fullWidth={true}
          required
        />
      </div>

      <div className={"Fields"}>
        <TextField
          name={"field"}
          placeholder={"Field of study..."}
          label={"Field"}
          onChange={e => (education.field = e.target.value)}
          defaultValue={props.field}
          fullWidth={true}
          required
        />
      </div>

      <div className={"pl-2 pt-1 row "}>
        <div className={"col-md-6"}>
          <TextField
            name={"startDate"}
            label={"Start date"}
            type={"date"}
            onChange={e => (education.startDate = e.target.value)}
            defaultValue={Date.now()}
            fullWidth={true}
            required
          />
        </div>
        <div className={"col-md-6"}>
          <TextField
            name={"endDate"}
            label={"End date"}
            type={"date"}
            onChange={e => (education.endDate = e.target.value)}
            defaultValue={Date.now()}
            fullWidth={true}
            required
          />
        </div>
      </div>

      <div className={"Fields d-flex justify-content-end"}>
        <Button
          variant="contained"
          color={"primary"}
          type={"submit"}
          className={"primary "}
        >
          Update
        </Button>
      </div>

      <div className={""}>
        <Button color={"secondary"} onClick={() => props.editEducation()}>
          Cancel
        </Button>
      </div>
    </form>
  );
}
