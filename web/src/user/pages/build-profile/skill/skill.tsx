import React, {useEffect, useState} from "react";
import {
  Button,
  Card,
  FormControl,
  InputLabel,
    LinearProgress,
  MenuItem,
  Select,
  TextField
} from "@material-ui/core";
import "./skill.scss";
import Edit from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import Axios from "axios";

interface ISkillProps {
  skill: [{ skill: String; level: String }];
  talentId: string;
}

export function Skills(props: ISkillProps) {
    let [resp, setResp] = useState();
  let [skill, setSkill] = useState();
    let [level, setLevel] = useState("Beginner");
  let [skillForm, setSkillForm] = useState(false);
  let addSkill = function() {
      Axios.put("/api/talent/skill/add", {skill: skill, level: level})
          .then(() => {
              setSkillForm(false);
              getProfile();
          })
          .catch();
  };
    let getProfile = function () {
        Axios.get("/api/talent/mine")
            .then(resp => {
                setResp(resp.data);
            })
            .catch(console.error);
    };

    useEffect(() => {
        getProfile();
    }, []);
    return resp ? (
    <Card className={"skill-form"}>
      <span className={"card-title"}> #Skills </span>
      <div className={"Fields"}>
        <span className={"d-flex justify-content-between"}>
          <span className={"title"}> Skills </span>
          <span
            style={{ display: !skillForm ? "block" : "none" }}
            className={"addButton"}
            onClick={() => setSkillForm(!skillForm)}
          >
            {" "}
            +{" "}
          </span>
        </span>
        <hr />
        <div className={"block "}>
          <div>
            <div className={""}>
                {resp!.skills!.length > 0 ? (
                <div>
                    {resp.skills!.map((skill: any) => (
                    <SkillTile
                        talentId={props.talentId}
                        _id={skill._id}
                        skill={skill.skill}
                        level={skill.level}
                        update={() => getProfile()}
                    />
                  ))}
                </div>
              ) : (
                <h6
                  className={"m-2"}
                  style={{ display: !skillForm ? "block" : "none" }}
                >
                  {" "}
                  None{" "}
                </h6>
              )}
            </div>
            <div style={{ display: skillForm ? "block" : "none" }}>
                <form
                    onSubmit={e => {
                        e.preventDefault();
                        addSkill();
                    }}
                >
                <div className={"Fields"}>
                  <span className={"card-title"}> eg. Nodejs </span>
                  <TextField
                    name={"skill"}
                    placeholder={"Add your skill"}
                    label={"Skill"}
                    onChange={e => setSkill(e.target.value)}
                    fullWidth={true}
                    required
                  />
                </div>
                <div className={"Fields"}>
                  <FormControl className={"full-width"}>
                    <InputLabel htmlFor="Level">Level</InputLabel>
                    <Select
                        fullWidth={true}
                        value={level}
                        onChange={e => setLevel(e.target.value as string)}
                        inputProps={{
                        id: "Level"
                      }}
                        required
                    >
                        <MenuItem value={"Beginner"} selected={true}>
                            Beginner
                        </MenuItem>
                      <MenuItem value={"Advanced"}>Advanced</MenuItem>
                      <MenuItem value={"Expert"}>Expert</MenuItem>
                    </Select>
                    <div className={"Fields d-flex justify-content-end"}>
                      <Button
                        variant="contained"
                        color={"primary"}
                        type={"submit"}
                        className={"primary "}
                        onClick={() => {}}
                      >
                        {" "}
                        Add{" "}
                      </Button>
                    </div>
                  </FormControl>
                </div>
                <div className={""}>
                  <Button
                    color={"secondary"}
                    onClick={() => setSkillForm(!skillForm)}
                  >
                    Cancel
                  </Button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </Card>
    ) : (
        <LinearProgress/>
    );
}

function EditSkill(props: {
  _id: string;
  skill: string;
  level: string;
  editSkillForm: () => void;
  talentId: string;
    update: () => void;
}) {
  let [skill, setSkill] = useState(props.skill);
  let [level, setLevel] = useState(props.level);

  let editSkill = function() {
    Axios.put(`/api/talent/${props.talentId}/skill/${props._id}`, {
      skill: skill,
      level: level
    })
        .then(() => {
            props.update();
            props.editSkillForm();
        })
      .catch();
  };

  return (
    <form
      onSubmit={e => {
        e.preventDefault();
        editSkill();
      }}
    >
      <div className={"Fields"}>
        <TextField
          name={"skill"}
          label={"Update skill"}
          defaultValue={props.skill}
          onChange={e => setSkill(e.target.value)}
          fullWidth={true}
          required
        />
      </div>
      <div className={"Fields"}>
        <FormControl className={"full-width"}>
          <InputLabel htmlFor="Level">Level</InputLabel>
          <Select
            value={level}
            fullWidth={true}
            // value={}
            //                        @ts-ignore
            onChange={e => setLevel(e.target.value)}
            inputProps={{
              id: "Level"
            }}
            required
          >
            <MenuItem value={"Beginner"}>Beginner</MenuItem>
            <MenuItem value={"Advanced"}>Advanced</MenuItem>
            <MenuItem value={"Expert"}>Expert</MenuItem>
          </Select>
          <div className={"Fields d-flex justify-content-end"}>
            <Button
              variant="contained"
              color={"primary"}
              type={"submit"}
              className={"primary "}
              onClick={() => {}}
            >
              {" "}
              Update{" "}
            </Button>
          </div>
        </FormControl>
      </div>
      <div className={""}>
        <Button color={"secondary"} onClick={() => props.editSkillForm()}>
          Cancel
        </Button>
      </div>
    </form>
  );
}

function SkillTile(props: {
  level: string;
  skill: string;
  _id: string;
  talentId: string;
    update: () => void;
}) {
  let [editSkill, setEditSkill] = useState(false);
  let deleteSkill = function() {
    Axios.delete(`/api/talent/${props.talentId}/skill/${props._id}`)
        .then(() => props.update())
      .catch();
  };
  return (
    <div className={"pt-1 skill-list full-width"}>
      {editSkill ? (
        <EditSkill
            talentId={props.talentId}
            _id={props._id}
            skill={props.skill}
            level={props.level}
            update={() => props.update()}
            editSkillForm={() => setEditSkill(!setEditSkill)}
        />
      ) : (
        <>
          <span className={"skill-name"}>
            {" "}
            {props.skill}
            <span className={"level"}> ({props.level} ) </span>
          </span>
          <span className={"right"}>
            <Edit
              color={"primary"}
              className={"pointer"}
              onClick={() => setEditSkill(true)}
            />
            &nbsp;
            <DeleteIcon
              className={"trash-icon pointer"}
              onClick={() => deleteSkill()}
            />
          </span>
        </>
      )}
    </div>
  );
}
