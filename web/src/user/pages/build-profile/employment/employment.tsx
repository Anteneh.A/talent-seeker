import React, {useEffect, useState} from "react";
import {Button, Card, LinearProgress, Switch, TextField} from "@material-ui/core";
import "./employment.scss";
import Edit from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import Axios from "axios";
import Calendar from "@material-ui/icons/CalendarToday";

interface IEmploymentProps {
  talentId: string;
  employment: [
    {
      company: { type: String; required: true };
      startDate: Date;
      endDate: Date;
      jobTitle: string;
      description: string;
    }
  ];
}

const data = [
  {
    company: "MMCY tech",
    startDate: "June 2019",
    endDate: "Present",
    jobTitle: "Developer",
    description: "Some description goes here"
  },
  {
    company: "UQO.io",
    startDate: "April 2019",
    endDate: "June 2019",
    jobTitle: "Mobile app developer",
    description: "Some description goes here about the job"
  }
];

export function Employment(props: IEmploymentProps) {
  let [resp, setResp] = useState();
  let [employmentForm, setemploymentForm] = useState(false);
  let [employment, setemployment] = useState({
    company: "",
    startDate: "",
    endDate: "",
    jobTitle: "",
    description: ""
  });
  let add = function() {
    Axios.put(
      "/api/talent/employment/add",
      JSON.parse(JSON.stringify(employment))
    )
        .then(() => {
          setemploymentForm(false);
          getProfile()
        })
      .catch();
  };
  let getProfile = function () {
    Axios.get("/api/talent/mine")
        .then(resp => {
          setResp(resp.data);
        })
        .catch(console.error);
  };

  useEffect(() => {
    getProfile();
  }, []);
  return resp ? (
    <Card className={"skill-form"}>
      <span className={"card-title"}> #Employment</span>
      <div className={"Fields"}>
        <span className={"d-flex justify-content-between"}>
          <span className={"title"}> Employment history</span>
          <span
            style={{ display: !employmentForm ? "block" : "none" }}
            className={"addButton"}
            onClick={() => setemploymentForm(!employmentForm)}
          >
            {" "}
            +{" "}
          </span>
        </span>
        <hr />
        <div className={"block "}>
          <div>
            <div>
              {resp.employment.length > 0 ? (
                <div>
                  {resp.employment.map((employment: any) => (
                    <EmploymentTile
                        _id={employment._id}
                        jobTitle={employment.jobTitle}
                        talentId={props.talentId}
                        startDate={employment.startDate}
                        endDate={employment.endDate}
                        update={() => getProfile()}
                        description={employment.description}
                        company={employment.company}
                    />
                  ))}
                </div>
              ) : (
                <h6
                  className={"m-2"}
                  style={{ display: !employmentForm ? "block" : "none" }}
                >
                  {" "}
                  None{" "}
                </h6>
              )}
            </div>
            <div style={{ display: employmentForm ? "block" : "none" }}>
              <form
                onSubmit={e => {
                  e.preventDefault();
                  add();
                }}
              >
                <div className={"Fields"}>
                  <TextField
                    name={"company"}
                    placeholder={"Company name..."}
                    onChange={e => (employment.company = e.target.value)}
                    label={"Company"}
                    fullWidth={true}
                    required
                  />
                </div>

                <div className={"Fields"}>
                  <TextField
                    name={"jobTitle"}
                    placeholder={"Job title..."}
                    label={"Job Title"}
                    onChange={e => (employment.jobTitle = e.target.value)}
                    fullWidth={true}
                    required
                  />
                </div>

                <div className={"Fields"}>
                  <TextField
                    name={"description"}
                    placeholder={"Description..."}
                    label={"Description"}
                    onChange={e => (employment.description = e.target.value)}
                    fullWidth={true}
                    required
                  />
                </div>

                <div className={"row pl-2"}>
                  <div className={"col-md-6 col-s-12"}>
                    <TextField
                      name={"startDate"}
                      label={"Start Date"}
                      type={"date"}
                      onChange={e => (employment.startDate = e.target.value)}
                      fullWidth={true}
                      required
                    />
                  </div>
                  <div className={"col-md-6 col-s-12"}>
                    <TextField
                      name={"endDate"}
                      label={"End Date"}
                      type={"date"}
                      onChange={e => (employment.endDate = e.target.value)}
                      defaultValue={Date.now()}
                      fullWidth={true}
                      required
                    />
                  </div>
                </div>
                <br />

                <div className={"Fields d-flex justify-content-end"}>
                  <Button
                    variant="contained"
                    color={"primary"}
                    type={"submit"}
                    className={"primary "}
                  >
                    {" "}
                    Add{" "}
                  </Button>
                </div>

                <div className={""}>
                  <Button
                    color={"secondary"}
                    onClick={() => setemploymentForm(!employmentForm)}
                  >
                    Cancel
                  </Button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </Card>
  ) : <LinearProgress/>
}

function EmploymentTile(props: {
  _id: string;
  company: string;
  jobTitle: string;
  startDate: string;
  endDate: string;
  talentId: string;
  description: string;
  update: () => void
}) {
  let [editEmployment, setEditEmployment] = useState(false);
  let deleteJob = function() {
    Axios.delete(`/api/talent/${props.talentId}/employment/${props._id}`)
        .then(() => props.update())
        .catch();
  };
  return (
    <>
      {editEmployment ? (
        <EditEmployment
            _id={props._id}
            startDate={props.startDate}
            endDate={props.endDate}
            jobTitle={props.jobTitle}
            company={props.company}
            talentId={props.talentId}
            update={() => props.update()}
            description={props.description}
            editEmploymentform={() => setEditEmployment(!editEmployment)}
        />
      ) : (
        <>
          <div className={"pt-2 d-flex justify-content-between"}>
            <span>
              <div className={"field-name"}>
                {" "}
                {props.jobTitle}&nbsp;(
                <span className={"company-name"}>{props.company}</span>)
              </div>
              <div className={"employment-date pl-2"}>
                <Calendar style={{ fontSize: "14px" }} />
                &nbsp;{new Date(props.startDate).toDateString()} to{" "}
                {new Date(props.endDate).toDateString()}{" "}
              </div>
            </span>
            <span>
              <Edit
                color={"primary"}
                className={"pointer"}
                onClick={() => setEditEmployment(true)}
              />
              &nbsp;
              <DeleteIcon
                  className={"trash-icon pointer"}
                  onClick={() => deleteJob()}
              />
            </span>
          </div>
        </>
      )}
    </>
  );
}

function EditEmployment(props: {
  _id: string;
  jobTitle: string;
  company: string;
  startDate: string;
  endDate: string;
  talentId: string;
  description: string;
  editEmploymentform: () => void;
  update: () => void
}) {
  let [employment, setemployment] = useState({
    company: props.company,
    startDate: props.startDate,
    endDate: props.endDate,
    jobTitle: props.jobTitle,
    description: props.description
  });
  let edit = function() {
    Axios.put(
      `/api/talent/${props.talentId}/employment/${props._id}`,
      JSON.parse(JSON.stringify(employment))
    )
        .then(() => {
          props.editEmploymentform();
          props.update();
        })
      .catch();
  };
  return (
      <form
          onSubmit={e => {
            e.preventDefault();
            edit();
          }}
      >
      <div className={"Fields"}>
        <TextField
            name={"company"}
            placeholder={"Company name..."}
            label={"Company"}
            defaultValue={props.company}
            onChange={e => (employment.company = e.target.value)}
            fullWidth={true}
            required
        />
      </div>

      <div className={"Fields"}>
        <TextField
          name={"jobTitle"}
          placeholder={"Job title..."}
          label={"Job Title"}
          fullWidth={true}
          onChange={e => (employment.jobTitle = e.target.value)}
          defaultValue={props.jobTitle}
          required
        />
      </div>

      <div className={"Fields"}>
        <TextField
            name={"description"}
            label={"Description"}
            onChange={e => (employment.description = e.target.value)}
            fullWidth={true}
            multiline={true}
            rows={2}
            defaultValue={props.description}
            required
        />
      </div>

      <div className={"row pl-2"}>
        <div className={"col-md-6 col-s-12"}>
          <TextField
            name={"startDate"}
            label={"Start Date"}
            type={"date"}
            onChange={e => (employment.startDate = e.target.value)}
            defaultValue={Date.now()}
            fullWidth={true}
            required
          />
        </div>
        <div className={"col-md-6 col-s-12"}>
          <TextField
            name={"endDate"}
            label={"End Date"}
            type={"date"}
            onChange={e => (employment.endDate = e.target.value)}
            defaultValue={Date.now()}
            fullWidth={true}
            required
          />
        </div>
      </div>
      <br />

      <div className={"Fields d-flex justify-content-end"}>
        <Button
          variant="contained"
          color={"primary"}
          type={"submit"}
          className={"primary "}
          onClick={() => {}}
        >
          {" "}
          Update{" "}
        </Button>
      </div>

      <div className={""}>
        <Button color={"secondary"} onClick={() => props.editEmploymentform()}>
          Cancel
        </Button>
      </div>
    </form>
  );
}
