import React, { PropsWithChildren } from "react"
import { UserProvider } from "../stores/user-provider";

export default function Provider({ children }: PropsWithChildren<{}>) {
    return (
        <UserProvider>
            {children}
        </UserProvider>
    )
}