import React, {useEffect, useState} from "react";
import {Route, Switch} from "react-router";
import NotFound from "../../shared/pages/not-found/not-found";
import {useAccountState} from "../../app/stores/account/account-provider";
import {Login} from "../../shared/pages/login/login";
import {Register} from "../../shared/pages/register/register";
import {BuildProfile} from "../pages/build-profile/buid-profile";
import {ViewProfile} from "../pages/view-profile/view-profile";
import {Messages} from "../../shared/pages/messages/messages";
import {MessageDetail} from "../../shared/pages/message-detail/message-detail";
import Profile from "../../shared/pages/profile/profile";
import {Landing} from "../../shared/pages/landing/landing";
import {Home} from "../../recruiter/pages/home/home";
import {Search} from "../../shared/pages/search/search";
import Axios from "axios";

export function UserRoutes({prefix: p, account}: { prefix: string, account: any }) {
    // let [account, setAccount] = useState();
    return account ? (
        <Switch>
            {account!.role === "Employee" && (
                <>
                    <Route exact path={`${p}/`} component={BuildProfile}/>
                    <Route exact path={`${p}/talent/:_id`} component={ViewProfile}/>
                    <Route exact path={`${p}/messages`} component={Search}/>
                    <Route exact path={`${p}/message/:_id`} component={MessageDetail}/>
                    <Route exact path={`${p}/account`} component={Profile}/>
                </>
            )}

            {account!.role === "Recruiter" && (
                <>
                    <Route exact path={`${p}/`} component={Home}/>
                    <Route exact path={`${p}/talent/:_id`} component={ViewProfile}/>
                    <Route exact path={`${p}/messages`} component={Messages}/>
                    <Route exact path={`${p}/message/:_id`} component={MessageDetail}/>
                    <Route exact path={`${p}/account`} component={Profile}/>
                    <Route exact path={`${p}/search`} component={Search}/>
                </>
            )}
        </Switch>
    ) : <Switch>
        <Route exact path={`${p}/login`} component={Login}/>
        <Route exact path={`${p}/register`} component={Register}/>
        <Route exact path={`${p}/talent/:_id`} component={ViewProfile}/>
        <Route exact path={`${p}/search`} component={Search}/>
        <Route exact path={`${p}/`} component={Landing}/>
        <Route component={NotFound}/>
    </Switch>
}

/*
* : (
        <Switch>
            <Route exact path={`${p}/login`} component={Login}/>
            <Route exact path={`${p}/register`} component={Register}/>
            <Route exact path={`${p}/`} component={Landing}/>
            <Route component={NotFound}/>
        </Switch>
    );
*
* */